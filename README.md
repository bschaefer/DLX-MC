![License GPLv3](https://go-shields.herokuapp.com/License-GPLv3-green.png)

DLX_MULTI_CYCLE (DLX-MC) is a VHDL description of the multicycle implementation of the DLX processor.

# Testing
For testing DLX-MC needs to be initialized with bytecode. To generate compatible bytecode you can use [DLXAssembler](https://gitlab.com/bschaefer/DLXAssembler).

For more information please refer to the documentation in the folder [documentation](https://gitlab.com/bschaefer/DLX-MC/Dokumentation/dokumentation.pdf). (Currently only available in German.)