...
outer_loop: ; for(r4 = n; r4 > 0; --r4)
beqz r4, end_outer_loop
    addi r3, r1, 4 ; r3 = start address + 4
    add r8, r0, r0 ; swapped = 0 (false)

    inner_loop: ; for(r3 = 1; r3 < r4; ++r3)
    slt r7, r3, r4
    beqz r7, end_inner_loop
        ; load values
        lw r5, 0(r3); var1 = value[r3]
        lw r6, 4(r3); var2 = value[r3 + 1 word]
        
        ; compare values
        slt r7, r5, r6 ; r7 = (r5 < r6) ? 1 : 0
        
        bnez r7, no_swap;
        ; swap and store values
        sw 0(r3), r6
        sw 4(r3), r5
        addi r8, r0, 1 ; swapped = 1 (true)
        
        no_swap:
        addi r3, r3, 4 ; increment r3 one word
    j inner_loop
    end_inner_loop:
    
    ; if !swapped => list is sorted
    beqz r8, prog_end
    
    subi r4, r4, 4 ; decrement r4 one word
j outer_loop
end_outer_loop:
...