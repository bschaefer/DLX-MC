If you want to extend this project you can refer to the information in the [documentation](https://gitlab.com/bschaefer/DLX-MC/Dokumentation/dokumentation.pdf) which features could be implemented.

Also see the pages of our [wiki](https://gitlab.com/bschaefer/DLX-MC/wikis/home).