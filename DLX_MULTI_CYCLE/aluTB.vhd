-------------------------------------------------------------------------------
-- Modul: 						aluTb
-- Hauptveranwortlicher: 	jgreilich
-- Letzte Anderung:			07.12.2015
-- Beschreibung:
--	<beschreibung>
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
use work.dlx_package.all;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY aluTB IS
END aluTB;
 
ARCHITECTURE behavior OF aluTB IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT alu
    PORT(
			i_op_a :  in STD_LOGIC_VECTOR(0 to 31);
			i_op_b:  in STD_LOGIC_VECTOR(0 to 31);
			i_operation : in T_ALU_OPERATION;
			o_flags : out T_ALU_FLAGS;
			o_result : out STD_LOGIC_VECTOR(0 to 31)
    );
    END COMPONENT;
	 
    

   --Inputs
   signal i_op_a : std_logic_vector(0 to 31) := (others => '0');
   signal i_op_b : std_logic_vector(0 to 31) := (others => '0');
   signal i_operation : T_ALU_OPERATION := add;

 	--Outputs
   signal o_flags : T_ALU_FLAGS;
   signal o_result : std_logic_vector(0 to 31);


 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: alu PORT MAP (
          i_op_a => i_op_a,
          i_op_b => i_op_b,
          i_operation => i_operation,
          o_flags => o_flags,
          o_result => o_result
        );
 

   -- Stimulus process
   stim_proc: process
   begin		
report "######################  Beginn der Testbench";
		-- start-signals
		i_op_a <=  "10011110100111001011110010011101"; -- Test Value 
		i_operation <= shift_left_logic;


report "-------- Shift Left Logical ----------------------------";
-- 1: sll 0 -> No shift
		-- assign new shift amount
		i_op_b <= std_logic_vector(to_unsigned(0,32));
		-- hold state for 100 ns.
		wait for 50 ns;
		report "Step 1";
		assert o_result = "10011110100111001011110010011101" report "Result wrong." severity failure;
		assert o_flags.zero = '0' report "Zero-Flag wrong!" severity failure;
		wait for 50 ns;
		
-- 2: sll 1 -> single shift works?
		-- assign new shift amount
		i_op_b <= std_logic_vector(to_unsigned(1,32));
		-- hold state for 100 ns.
		wait for 50 ns;
		report "Step 2";
		assert o_result = "00111101001110010111100100111010" report "Result wrong." severity failure;
		assert o_flags.zero = '0' report "Zero-Flag wrong!" severity failure;
		wait for 50 ns;
		
-- 3: sll 17 -> shift amount is unsigned?
		-- assign new shift amount
		i_op_b <= std_logic_vector(to_unsigned(17,32));
		-- hold state for 100 ns.
		wait for 50 ns;
		report "Step 3";
		assert o_result = "01111001001110100000000000000000" report "Result wrong." severity failure;
		assert o_flags.zero = '0' report "Zero-Flag wrong!" severity failure;
		wait for 50 ns;
		
-- 4: sll 32 -> should be the same as zero
		-- assign new shift amount
		i_op_b <= std_logic_vector(to_unsigned(32,32));
		-- hold state for 100 ns.
		wait for 50 ns;
		report "Step 4";
		assert o_result = "10011110100111001011110010011101" report "Result wrong." severity failure;
		assert o_flags.zero = '0' report "Zero-Flag wrong!" severity failure;
		wait for 50 ns;

-- 5: sll 36 -> all except the lowest 5 bits are ignored? should be the same as sll 4
		-- assign new shift amount
		i_op_b <= std_logic_vector(to_unsigned(36,32));
		-- hold state for 100 ns.
		wait for 50 ns;
		report "Step 5";
		assert o_result = "11101001110010111100100111010000" report "Result wrong." severity failure;
		assert o_flags.zero = '0' report "Zero-Flag wrong!" severity failure;
		wait for 50 ns;

report " Test passed.";

report "-------- Shift Right Logical ----------------------------";
report ".. no test implemented yet ..";
	
report "-------- Add Signed -------------------------------------";

		i_operation <= add;
		 
		for i in -200 to 200 loop
			for j in -3 to 3 loop
				i_op_a <= std_logic_vector(to_signed(i,32));
				i_op_b <= std_logic_vector(to_signed(j,32));
				wait for 10 ns;
				assert signed(o_result) = i+j report "Error with " & integer'image(i) & "+" & integer'image(j) severity failure;
				wait for 10 ns;
			end loop;
		end loop;
		
		report "Test passed.";	


report "-------- Multiply Signed ---------------------------------";
		
		i_operation <= multiply_signed;

		for i in -200 to 200 loop
			for j in -3 to 3 loop
				i_op_a <= std_logic_vector(to_signed(i,32));
				i_op_b <= std_logic_vector(to_signed(j,32));
				wait for 10 ns;
				assert signed(o_result) = i*j report "Error with " & integer'image(i) & "*" & integer'image(j) severity failure;
				wait for 10 ns;
			end loop;
		end loop;
		
		report "Test passed.";
		
		
		
report "-------- Multiply Unsinged ---------------------------------";
		
		i_operation <= multiply_unsigned;

		for i in 0 to 200 loop
			for j in 0 to 6 loop
				i_op_a <= std_logic_vector(to_unsigned(i,32));
				i_op_b <= std_logic_vector(to_unsigned(j,32));
				wait for 10 ns;
				assert unsigned(o_result) = i*j report "Error with " & integer'image(i) & "*" & integer'image(j) severity failure;
				wait for 10 ns;
			end loop;
		end loop;

		report "Test passed.";
		
assert false report "######################  Ende der Testbench" severity failure;
   end process;

END;
