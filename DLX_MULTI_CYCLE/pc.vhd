-------------------------------------------------------------------------------
-- DLX_MULTI_CYCLE (DLX-MC)
-- Copyright 2016
-- https://gitlab.com/bschaefer/DLX-MC
--
-- This file is part of DLX-MC.
-- 
-- DLX-MC is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- DLX-MC is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with DLX-MC.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------
-- Module: 						pc
-- Original author: 			jgreilich
-- Co-authors:					
-- Last changed:				25.06.2015
-- Description:
--	Register used as pc
--
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity pc is
	port(
		i_clk:	 	in  STD_LOGIC;
		i_reset:	 	in  STD_LOGIC;
		i_address:	in  STD_LOGIC_VECTOR(0 to 31);
		i_enable: 	in  STD_LOGIC;
		o_address:	out STD_LOGIC_VECTOR(0 to 31)
	);
end pc;


architecture Behavioral of pc is

component register_general is
	port(
		i_clk: 	in  STD_LOGIC;
		i_reset: in  STD_LOGIC;
		i_en: 	in  STD_LOGIC;
		i_data: 	in  STD_LOGIC_VECTOR(0 to 31);
		o_data: 	out STD_LOGIC_VECTOR(0 to 31)
	);
end component register_general;

begin

regn : register_general
	port map(
		i_clk => i_clk,
		i_reset => i_reset,
		i_en => i_enable,
		i_data => i_address,
		o_data => o_address
	);

end Behavioral;