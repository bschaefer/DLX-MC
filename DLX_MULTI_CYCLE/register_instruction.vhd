-------------------------------------------------------------------------------
-- DLX_MULTI_CYCLE (DLX-MC)
-- Copyright 2016
-- https://gitlab.com/bschaefer/DLX-MC
--
-- This file is part of DLX-MC.
-- 
-- DLX-MC is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- DLX-MC is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with DLX-MC.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------
-- Module: 						register_instruction
-- Original author: 			jgreilich
-- Co-authors:					
-- Last changed:				25.06.2015
-- Description:
-- register holding current instruction outputting all necessary parts
--
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity register_instruction is
	port(
		i_clk: 				in STD_LOGIC;
		i_reset: 			in STD_LOGIC;
		i_r_i_write: 		in STD_LOGIC; -- IRWrite
		i_instruction: 	in STD_LOGIC_VECTOR(0 to 31);
		-- all Types
		o_opcode:			out STD_LOGIC_VECTOR(0 to 5); -- [0...5]
		-- only J-Type
		o_jump_address: 	out STD_LOGIC_VECTOR(0 to 25); -- [6...31]
		-- I-Type & R-Type
		o_rs:				 	out STD_LOGIC_VECTOR(0 to 4); -- [6...10]
		o_rt: 				out STD_LOGIC_VECTOR(0 to 4); -- [11...15]
		-- only R-Type
		o_rd:					out STD_LOGIC_VECTOR(0 to 4); -- [16...20]
		o_shammt:			out STD_LOGIC_VECTOR(0 to 4); -- [21...25]
		o_fcode:				out STD_LOGIC_VECTOR(0 to 5); -- [26...31]
		-- only I-Type
		o_immd:				out STD_LOGIC_VECTOR(0 to 15) -- [16...31]
	);
end register_instruction;

architecture Behavioral of register_instruction is

-- Register Output
signal w_instruction:	STD_LOGIC_VECTOR(0 to 31);

component register_general is
	port(
		i_clk: 	in  STD_LOGIC;
		i_reset: in  STD_LOGIC;
		i_en: 	in  STD_LOGIC;
		i_data: 	in  STD_LOGIC_VECTOR(0 to 31);
		o_data: 	out STD_LOGIC_VECTOR(0 to 31)
	);
end component;

begin

regn : register_general
	port map(
		i_clk => i_clk,
		i_reset => i_reset,
		i_en => i_r_i_write,
		i_data => i_instruction,
		o_data => w_instruction
	);
	
-- Instruction Splitting
o_opcode <= 		w_instruction(0 to 5);
o_jump_address <= w_instruction(6 to 31);
o_rs <= 				w_instruction(6 to 10);
o_rt <= 				w_instruction(11 to 15);
o_rd <= 				w_instruction(16 to 20);
o_shammt <= 		w_instruction(21 to 25);
o_fcode <= 			w_instruction(26 to 31);
o_immd <= 			w_instruction(16 to 31);

end Behavioral;