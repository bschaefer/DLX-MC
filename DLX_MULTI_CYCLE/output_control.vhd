-------------------------------------------------------------------------------
-- DLX_MULTI_CYCLE (DLX-MC)
-- Copyright 2016
-- https://gitlab.com/bschaefer/DLX-MC
--
-- This file is part of DLX-MC.
-- 
-- DLX-MC is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- DLX-MC is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with DLX-MC.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------
-- Module: 						output_control
-- Original author: 			bschaefer
-- Co-authors:					
-- Last changed:				17.12.2015
-- Description:
--	Transitions of the FSM modelling the output_control.
--
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

library work;
use work.dlx_package.all;
use work.output_control_package.all;


entity output_control is
	port(
		i_clk:						in STD_LOGIC;
		i_opcode:					in STD_LOGIC_VECTOR(0 to 5);
		o_pc_write_cond: 			out STD_LOGIC;
		o_inv_zero_flag: 			out STD_LOGIC;
		o_pc_write: 				out STD_LOGIC;
		o_i_or_d: 					out T_I_OR_D; -- instruction or data
		o_mem_read: 				out STD_LOGIC;
		o_mem_write: 				out STD_LOGIC;
		o_mem_to_reg: 				out STD_LOGIC;
		o_r_i_write: 				out STD_LOGIC; -- IRWrite
		o_pc_source: 				out T_PC_SOURCE;
		o_alu_op: 					out T_ALU_OP;
		o_alu_src_b: 				out T_ALU_SRC_B;
		o_alu_src_a: 				out T_ALU_SRC_A;
		o_reg_write: 				out STD_LOGIC;
		o_reg_dst: 					out T_REG_DST
		
	);

end output_control;

architecture Behavioral of output_control is
	signal w_out : T_OUT_STATE := INSTRUCTION_FETCH_S;
begin

	
	output:
	process (w_out) is
	begin
	o_pc_write_cond 			<= w_out.pc_write_cond;
	o_inv_zero_flag			<= w_out.inv_zero_flag;
	o_pc_write 					<= w_out.pc_write;
	o_i_or_d 					<= w_out.i_or_d;
	o_mem_read					<= w_out.mem_read;
	o_mem_write					<= w_out.mem_write;
	o_mem_to_reg				<= w_out.mem_to_reg;
	o_r_i_write					<= w_out.r_i_write;
	o_pc_source					<= w_out.pc_source;
	o_alu_op						<= w_out.alu_op;
	o_alu_src_b					<= w_out.alu_src_b;
	o_alu_src_a					<= w_out.alu_src_a;
	o_reg_write					<= w_out.reg_write;
	o_reg_dst					<= w_out.reg_dst;
	end process;
	
	state:
	process (i_clk) is 
	begin
		if falling_edge(i_clk) then
			if INSTRUCTION_FETCH_S = w_out then
				w_out <= INSTRUCTION_DECODE_S;
			elsif INSTRUCTION_DECODE_S = w_out then
				-- i-Type instructions
				if get_type(i_opcode) = i then
					-- arithmetic
					if i_opcode = ADDI_OC then
						w_out <= EXECUTE_ADD_IMMEDIATE_S;
					elsif i_opcode = SUBI_OC then
						w_out <= EXECUTE_SUB_IMMEDIATE_S;
					-- shift
					elsif i_opcode = SLLI_OC then
						w_out <= EXECUTE_SLL_IMMEDIATE_S;
					elsif i_opcode = SRLI_OC then
						w_out <= EXECUTE_SRL_IMMEDIATE_S;
					elsif i_opcode = SRAI_OC then
						w_out <= EXECUTE_SRA_IMMEDIATE_S;
					-- logical
					elsif i_opcode = ANDI_OC then
						w_out <= EXECUTE_AND_IMMEDIATE_S;
					elsif i_opcode = ORI_OC then
						w_out <= EXECUTE_OR_IMMEDIATE_S;
					elsif i_opcode = XORI_OC then
						w_out <= EXECUTE_XOR_IMMEDIATE_S;
					-- branch
					elsif i_opcode = BEQZ_OC then
						w_out <= BRANCH_COMPLETION_S;
					elsif i_opcode = BNEZ_OC then
						w_out <= INV_BRANCH_COMPLETION_S;
					-- memory
					elsif i_opcode = SW_OC or i_opcode = LW_OC then
						w_out <= EXECUTE_MEM_ADDR_S;
					-- not implemented
					else 
						w_out <= NOT_IMPLEMENTED_S;
						-- synthesis off
						report("Unimplemented I-Type");
						-- synthesis on
					end if;
				-- j-Type instructions
				elsif is_j_type(i_opcode) then
					-- might be incorrect for opcodes other than J_OC
					w_out <= JUMP_COMPLETION_S;
				-- r-Type instructions (integer)
				elsif is_r_type_1(i_opcode) then
					w_out <= R_TYPE_EX_S;
				elsif is_r_type_2(i_opcode) then
					w_out <= R_FP_TYPE_EX_S;
				else 
					w_out <= NOT_IMPLEMENTED_S;
					-- synthesis off
						report("Unimplemented Type: " & t_instruction_type'image(get_type(i_opcode)));
						-- synthesis on
				end if;
			-- if immediate was executed
			elsif is_execute_immediate(w_out) then
				w_out <= WB_IMMEDIATE_S;
			-- if r_Type was executed
			elsif R_TYPE_EX_S = w_out then
				w_out <= R_TYPE_WB_S;
			elsif R_FP_TYPE_EX_S = w_out then
				w_out <= R_FP_TYPE_WB_S;
			-- if load/store was executed
			elsif EXECUTE_MEM_ADDR_S = w_out then
				case i_opcode is
					when SW_OC  => w_out <= MEM_SW_S;
					when LW_OC  => w_out <= MEM_LW_S;
					when others => w_out <= NOT_IMPLEMENTED_S; --should never happen
				end case;
			-- if LW was in MEM phase
			elsif MEM_LW_S = w_out then
				w_out <= WB_LW_S;
			-- if last state of opcode
			elsif is_followed_by_if(w_out) then
				w_out <= INSTRUCTION_FETCH_S;
			else 
				w_out <= NOT_IMPLEMENTED_S;
				-- synthesis off
						report("Unimplemented State");
				-- synthesis on
			end if;
		end if;
	end process;

end Behavioral;