-------------------------------------------------------------------------------
-- DLX_MULTI_CYCLE (DLX-MC)
-- Copyright 2016
-- https://gitlab.com/bschaefer/DLX-MC
--
-- This file is part of DLX-MC.
-- 
-- DLX-MC is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- DLX-MC is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with DLX-MC.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------
-- Module: 						shift_left_2
-- Original author: 			bschaefer
-- Co-authors:					
-- Last changed:				22.09.2015
-- Description:
--	Shifts input to left by 2 filling with zeros.
--
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


entity shift_left_2 is
	port(
		i_data : in STD_LOGIC_VECTOR(0 to 31);
		o_data : out STD_LOGIC_VECTOR(0 to 31);
		i_instruction : in STD_LOGIC_VECTOR(0 to 25);
		o_instruction : out STD_LOGIC_VECTOR(0 to 27)
	);
end shift_left_2;

architecture Behavioral of shift_left_2 is

	signal w_data : STD_LOGIC_VECTOR(0 to 31);
	signal w_instruction : STD_LOGIC_VECTOR(0 to 25);

begin

	input_data:
	w_data <= i_data;
	
	output_data:
	o_data <= w_data(0) & w_data(3 to 31) & "00";
	
	input_instruction:
	w_instruction <= i_instruction;
	
	output_instuction:
	o_instruction <= w_instruction & "00";


end Behavioral;

