-------------------------------------------------------------------------------
-- Modul: 				    shift_left_2
-- Hauptveranwortlicher: 	dgermann
-- Letzte Anderung:			12.05.2015
-- Beschreibung:
--	<beschreibung>
--
-------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY shift_left_2TB IS
END shift_left_2TB;
 
ARCHITECTURE behavior OF shift_left_2TB IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT shift_left_2
    PORT(
         i_data : IN  std_logic_vector(0 to 31);
         o_data : OUT  std_logic_vector(0 to 31);
         i_instruction : IN  std_logic_vector(0 to 25);
         o_instruction : OUT  std_logic_vector(0 to 27)
        );
    END COMPONENT;
    

   --Inputs
   signal i_data : std_logic_vector(0 to 31) := (others => '0');
   signal i_instruction : std_logic_vector(0 to 25) := (others => '0');

 	--Outputs
   signal o_data : std_logic_vector(0 to 31);
   signal o_instruction : std_logic_vector(0 to 27);
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: shift_left_2 PORT MAP (
          i_data => i_data,
          o_data => o_data,
          i_instruction => i_instruction,
          o_instruction => o_instruction
        );


   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.	
		report " Beginn der Testbench ";
		--data shift
		i_data <= (others => '1');
		wait for 10 ns;
		assert(o_data = "11111111111111111111111111111100") report" o_data ist nicht o_data=11111111111111111111111111111100"severity failure;
		i_data <= (others => '0');
		wait for 10 ns;
		assert(o_data = "00000000000000000000000000000000") report" o_data ist nicht o_data=00000000000000000000000000000000"severity failure;
		i_data <= ("11001100110011001100110011001100");
		wait for 10 ns;
		assert(o_data = "10110011001100110011001100110000") report" o_data ist nicht o_data=10110011001100110011001100110000" severity failure;
		--instruction shift
		i_instruction <= (others => '1');
		wait for 10 ns;
		assert(o_instruction = "1111111111111111111111111100") report" o_instruction ist nicht o_instruction=111111111111111111111111100"severity failure;
		i_instruction <= (others => '0');
		wait for 10 ns;
		assert(o_instruction = "0000000000000000000000000000") report" o_instruction ist nicht o_instruction=000000000000000000000000000"severity failure;
		i_instruction <= ("0011001100110011001100110011");
		wait for 10 ns;
		assert(o_instruction = "1100110011001100110011001100") report" o_instruction ist nicht o_instruction=1100110011001100110011001100"severity failure;
		report " Ende der Testbench "severity failure;
   end process;

END;
