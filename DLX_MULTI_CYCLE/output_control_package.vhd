-------------------------------------------------------------------------------
-- DLX_MULTI_CYCLE (DLX-MC)
-- Copyright 2016
-- https://gitlab.com/bschaefer/DLX-MC
--
-- This file is part of DLX-MC.
-- 
-- DLX-MC is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- DLX-MC is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with DLX-MC.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------
-- Package: 					output_control_package
-- Original author: 			bschaefer
-- Co-authors:					
-- Last changed:				24.02.2016
-- Description:
--	opcodes, functions for interpreting opcodes, states of output_control FSM
-- This package is for use by output_control only. All commonly used types, 
-- constants, functions, etc. should be declared in dlx_package.
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

library work;
use work.dlx_package.all;

package output_control_package is

type T_INSTRUCTION_TYPE is (j, r_1 , r_2, i);
type T_OUT_STATE is 
record
	nr : INTEGER;
	pc_write_cond: 			STD_LOGIC;
	inv_zero_flag: 			STD_LOGIC;
	pc_write:  					STD_LOGIC;
	i_or_d:  					T_I_OR_D; -- instruction or data
	mem_read:  					STD_LOGIC;
	mem_write:  				STD_LOGIC;
	mem_to_reg: 				STD_LOGIC;
	r_i_write: 					STD_LOGIC; -- IRWrite
	pc_source: 					T_PC_SOURCE;
	alu_op: 						T_ALU_OP;
	alu_src_b: 					T_ALU_SRC_B;
	alu_src_a: 					T_ALU_SRC_A;
	reg_write: 					STD_LOGIC;
	reg_dst: 					T_REG_DST;
end record;

-- states
constant NOT_IMPLEMENTED_S : T_OUT_STATE :=(
		nr => -1,
		pc_write_cond => 'U',
		inv_zero_flag => 'U',
		pc_write => 'U',
		i_or_d => instruction,--U
		mem_read => 'U',
		mem_write => 'U',
		mem_to_reg => 'U',
		r_i_write => 'U',
		pc_source => alu_result,--U
		alu_op => add,--U
		alu_src_b => four,--U
		alu_src_a => pc,--U
		reg_write => 'U',
		reg_dst => d); --U
		
		
constant INSTRUCTION_FETCH_S : T_OUT_STATE :=(
		nr => 1,
		pc_write_cond => '0',
		inv_zero_flag => '0',
		pc_write => '1',
		i_or_d => instruction,
		mem_read => '1',
		mem_write => '0',
		mem_to_reg => '0',
		r_i_write => '1',
		pc_source => alu_result,
		alu_op => add,
		alu_src_b => four,
		alu_src_a => pc,
		reg_write => '0',
		reg_dst => d); --U
		
constant INSTRUCTION_DECODE_S : T_OUT_STATE :=(
		nr => 2,
		pc_write_cond => '0',
		inv_zero_flag => '0',
		pc_write => '0',
		i_or_d => instruction, --U
		mem_read => '0',
		mem_write => '0',
		mem_to_reg => '0',
		r_i_write => '0',
		pc_source => jmp_adr, --U
		alu_op => add,
		alu_src_b => i_address,
		alu_src_a => pc,
		reg_write => '0',
		reg_dst => d); --U
		
constant EXECUTE_ADD_IMMEDIATE_S : T_OUT_STATE :=(
		nr => 3,
		pc_write_cond => '0',
		inv_zero_flag => '0',
		pc_write => '0',
		i_or_d => instruction, --U
		mem_read => '0',
		mem_write => '0',
		mem_to_reg => '0',
		r_i_write => '0',
		pc_source => jmp_adr, --U
		alu_op => add,
		alu_src_b => immediate,
		alu_src_a => reg_a,
		reg_write => '0',
		reg_dst => t); --U

constant WB_IMMEDIATE_S : T_OUT_STATE :=(
		nr => 4,
		pc_write_cond => '0',
		inv_zero_flag => '0',
		pc_write => '0',
		i_or_d => instruction, --U
		mem_read => '0',
		mem_write => '0',
		mem_to_reg => '0',
		r_i_write => '0',
		pc_source => jmp_adr, --U
		alu_op => add, --U
		alu_src_b => immediate, --U
		alu_src_a => reg_a, --U
		reg_write => '1',
		reg_dst => t);
		
constant BRANCH_COMPLETION_S : T_OUT_STATE :=(
		nr => 5,
		pc_write_cond => '1',
		inv_zero_flag => '0',
		pc_write => '0',
		i_or_d => instruction, --U
		mem_read => '0',
		mem_write => '0',
		mem_to_reg => '0',
		r_i_write => '0',
		pc_source => alu_out,
		alu_op => add, --TODO might need to be subtract; why?
		alu_src_b => reg_b,
		alu_src_a => reg_a,
		reg_write => '0',
		reg_dst => t); --U
		
constant INV_BRANCH_COMPLETION_S : T_OUT_STATE :=(
		nr => 6,
		pc_write_cond => '1',
		inv_zero_flag => '1',
		pc_write => '0',
		i_or_d => instruction, --U
		mem_read => '0',
		mem_write => '0',
		mem_to_reg => '0',
		r_i_write => '0',
		pc_source => alu_out,
		alu_op => add,
		alu_src_b => reg_b,
		alu_src_a => reg_a,
		reg_write => '0',
		reg_dst => t); --U
		
constant JUMP_COMPLETION_S : T_OUT_STATE :=(
		nr => 7,
		pc_write_cond => '0',
		inv_zero_flag => '0',
		pc_write => '1',
		i_or_d => instruction, --U
		mem_read => '0',
		mem_write => '0',
		mem_to_reg => '0',
		r_i_write => '0',
		pc_source => jmp_adr,
		alu_op => add, --U
		alu_src_b => reg_b, --U
		alu_src_a => pc, --U
		reg_write => '0',
		reg_dst => t); --U
		
constant R_TYPE_EX_S : T_OUT_STATE :=(
		nr => 8,
		pc_write_cond => '0',
		inv_zero_flag => '0',
		pc_write => '0',
		i_or_d => instruction, --U
		mem_read => '0',
		mem_write => '0',
		mem_to_reg => '0',
		r_i_write => '0',
		pc_source => jmp_adr,--U
		alu_op => r_type,
		alu_src_b => reg_b,
		alu_src_a => reg_a,
		reg_write => '0',
		reg_dst => d);
		
constant R_TYPE_WB_S : T_OUT_STATE :=(
		nr => 9,
		pc_write_cond => '0',
		inv_zero_flag => '0',
		pc_write => '0',
		i_or_d => instruction, --U
		mem_read => '0',
		mem_write => '0',
		mem_to_reg => '0',
		r_i_write => '0',
		pc_source => jmp_adr,--U
		alu_op => r_type,
		alu_src_b => reg_b, --U
		alu_src_a => pc,--U
		reg_write => '1',
		reg_dst => d);
		
constant EXECUTE_MEM_ADDR_S : T_OUT_STATE :=(
		nr => 10,
		pc_write_cond => '0',
		inv_zero_flag => '0',
		pc_write => '0',
		i_or_d => data,
		mem_read => '0',
		mem_write => '0',
		mem_to_reg => '0',
		r_i_write => '0',
		pc_source => jmp_adr,--U
		alu_op => add,
		alu_src_b => immediate,
		alu_src_a => reg_a,
		reg_write => '0',
		reg_dst => d);--U
		
constant MEM_SW_S : T_OUT_STATE :=(
		nr => 11,
		pc_write_cond => '0',
		inv_zero_flag => '0',
		pc_write => '0',
		i_or_d => data,
		mem_read => '0',
		mem_write => '1',
		mem_to_reg => '0',
		r_i_write => '0',
		pc_source => jmp_adr,--U
		alu_op => add,
		alu_src_b => immediate,
		alu_src_a => reg_a,
		reg_write => '0',
		reg_dst => d);--U	
		
constant MEM_LW_S : T_OUT_STATE :=(
		nr => 12,
		pc_write_cond => '0',
		inv_zero_flag => '0',
		pc_write => '0',
		i_or_d => data,
		mem_read => '1',
		mem_write => '0',
		mem_to_reg => '0',
		r_i_write => '0',
		pc_source => jmp_adr,--U
		alu_op => add, --U
		alu_src_b => immediate,
		alu_src_a => reg_a,
		reg_write => '0',
		reg_dst => t); --yet U
		
constant WB_LW_S : T_OUT_STATE :=(
		nr => 13,
		pc_write_cond => '0',
		inv_zero_flag => '0',
		pc_write => '0',
		i_or_d => data,
		mem_read => '0',
		mem_write => '0',
		mem_to_reg => '1',
		r_i_write => '0',
		pc_source => jmp_adr,--U
		alu_op => add, --U
		alu_src_b => immediate,
		alu_src_a => reg_a,
		reg_write => '1',
		reg_dst => t);
		
constant EXECUTE_SUB_IMMEDIATE_S : T_OUT_STATE :=(
		nr => 14,
		pc_write_cond => '0',
		inv_zero_flag => '0',
		pc_write => '0',
		i_or_d => instruction, --U
		mem_read => '0',
		mem_write => '0',
		mem_to_reg => '0',
		r_i_write => '0',
		pc_source => jmp_adr, --U
		alu_op => subtract,
		alu_src_b => immediate,
		alu_src_a => reg_a,
		reg_write => '0',
		reg_dst => t); --U
		
constant EXECUTE_AND_IMMEDIATE_S : T_OUT_STATE :=(
		nr => 15,
		pc_write_cond => '0',
		inv_zero_flag => '0',
		pc_write => '0',
		i_or_d => instruction, --U
		mem_read => '0',
		mem_write => '0',
		mem_to_reg => '0',
		r_i_write => '0',
		pc_source => jmp_adr, --U
		alu_op => and_logic,
		alu_src_b => immediate,
		alu_src_a => reg_a,
		reg_write => '0',
		reg_dst => t); --U
		
constant EXECUTE_OR_IMMEDIATE_S : T_OUT_STATE :=(
		nr => 16,
		pc_write_cond => '0',
		inv_zero_flag => '0',
		pc_write => '0',
		i_or_d => instruction, --U
		mem_read => '0',
		mem_write => '0',
		mem_to_reg => '0',
		r_i_write => '0',
		pc_source => jmp_adr, --U
		alu_op => or_logic,
		alu_src_b => immediate,
		alu_src_a => reg_a,
		reg_write => '0',
		reg_dst => t); --U
		
constant EXECUTE_XOR_IMMEDIATE_S : T_OUT_STATE :=(
		nr => 17,
		pc_write_cond => '0',
		inv_zero_flag => '0',
		pc_write => '0',
		i_or_d => instruction, --U
		mem_read => '0',
		mem_write => '0',
		mem_to_reg => '0',
		r_i_write => '0',
		pc_source => jmp_adr, --U
		alu_op => xor_logic,
		alu_src_b => immediate,
		alu_src_a => reg_a,
		reg_write => '0',
		reg_dst => t); --U

constant EXECUTE_SLL_IMMEDIATE_S : T_OUT_STATE :=(
		nr => 18,
		pc_write_cond => '0',
		inv_zero_flag => '0',
		pc_write => '0',
		i_or_d => instruction, --U
		mem_read => '0',
		mem_write => '0',
		mem_to_reg => '0',
		r_i_write => '0',
		pc_source => jmp_adr, --U
		alu_op => shift_left_logic,
		alu_src_b => immediate,
		alu_src_a => reg_a,
		reg_write => '0',
		reg_dst => t); --U
		
constant EXECUTE_SRL_IMMEDIATE_S : T_OUT_STATE :=(
		nr => 19,
		pc_write_cond => '0',
		inv_zero_flag => '0',
		pc_write => '0',
		i_or_d => instruction, --U
		mem_read => '0',
		mem_write => '0',
		mem_to_reg => '0',
		r_i_write => '0',
		pc_source => jmp_adr, --U
		alu_op => shift_right_logic,
		alu_src_b => immediate,
		alu_src_a => reg_a,
		reg_write => '0',
		reg_dst => t); --U
		
constant EXECUTE_SRA_IMMEDIATE_S : T_OUT_STATE :=(
		nr => 20,
		pc_write_cond => '0',
		inv_zero_flag => '0',
		pc_write => '0',
		i_or_d => instruction, --U
		mem_read => '0',
		mem_write => '0',
		mem_to_reg => '0',
		r_i_write => '0',
		pc_source => jmp_adr, --U
		alu_op => shift_right_arith,
		alu_src_b => immediate,
		alu_src_a => reg_a,
		reg_write => '0',
		reg_dst => t); --U
		
constant R_FP_TYPE_EX_S : T_OUT_STATE :=(
		nr => 21,
		pc_write_cond => '0',
		inv_zero_flag => '0',
		pc_write => '0',
		i_or_d => instruction, --U
		mem_read => '0',
		mem_write => '0',
		mem_to_reg => '0',
		r_i_write => '0',
		pc_source => jmp_adr,--U
		alu_op => fp_r_type,
		alu_src_b => reg_b,
		alu_src_a => reg_a,
		reg_write => '0',
		reg_dst => d);
		
constant R_FP_TYPE_WB_S : T_OUT_STATE :=(
		nr => 22,
		pc_write_cond => '0',
		inv_zero_flag => '0',
		pc_write => '0',
		i_or_d => instruction, --U
		mem_read => '0',
		mem_write => '0',
		mem_to_reg => '0',
		r_i_write => '0',
		pc_source => jmp_adr,--U
		alu_op => fp_r_type,
		alu_src_b => reg_b,
		alu_src_a => reg_a,
		reg_write => '1',
		reg_dst => d);
		
		

-- opcodes
constant SPECIAL_OC 	: STD_LOGIC_VECTOR(0 to 5) := "000000";
constant FPARITH_OC 	: STD_LOGIC_VECTOR(0 to 5) := "000001";
constant J_OC 			: STD_LOGIC_VECTOR(0 to 5) := "000010";
constant JAL_OC		: STD_LOGIC_VECTOR(0 to 5) := "000011";
constant BEQZ_OC 		: STD_LOGIC_VECTOR(0 to 5) := "000100";
constant BNEZ_OC 		: STD_LOGIC_VECTOR(0 to 5) := "000101";
constant BFPT_OC 		: STD_LOGIC_VECTOR(0 to 5) := "000110";
constant BFPF_OC 		: STD_LOGIC_VECTOR(0 to 5) := "000111";
constant ADDI_OC 		: STD_LOGIC_VECTOR(0 to 5) := "001000";
constant ADDUI_OC 	: STD_LOGIC_VECTOR(0 to 5) := "001001";
constant SUBI_OC		: STD_LOGIC_VECTOR(0 to 5) := "001010";
constant SUBUI_OC		: STD_LOGIC_VECTOR(0 to 5) := "001011";
constant ANDI_OC 		: STD_LOGIC_VECTOR(0 to 5) := "001100";
constant ORI_OC 		: STD_LOGIC_VECTOR(0 to 5) := "001101";
constant XORI_OC 		: STD_LOGIC_VECTOR(0 to 5) := "001110";
constant LHI_OC 		: STD_LOGIC_VECTOR(0 to 5) := "001111";
constant RFE_OC	 	: STD_LOGIC_VECTOR(0 to 5) := "010000";
constant TRAP_OC	 	: STD_LOGIC_VECTOR(0 to 5) := "010001";
constant JR_OC			: STD_LOGIC_VECTOR(0 to 5) := "010010";
constant JALR_OC		: STD_LOGIC_VECTOR(0 to 5) := "010011";
constant SLLI_OC 		: STD_LOGIC_VECTOR(0 to 5) := "010100";
--constant <>	 		: STD_LOGIC_VECTOR(0 to 5) := "010101";
constant SRLI_OC 		: STD_LOGIC_VECTOR(0 to 5) := "010110";
constant SRAI_OC 		: STD_LOGIC_VECTOR(0 to 5) := "010111";
constant SEQI_OC 		: STD_LOGIC_VECTOR(0 to 5) := "011000";
constant SNEI_OC	 	: STD_LOGIC_VECTOR(0 to 5) := "011001";
constant SLTI_OC		: STD_LOGIC_VECTOR(0 to 5) := "011010";
constant SGTI_OC		: STD_LOGIC_VECTOR(0 to 5) := "011011";
constant SLEI_OC 		: STD_LOGIC_VECTOR(0 to 5) := "011100";
constant SGEI_OC 		: STD_LOGIC_VECTOR(0 to 5) := "011101";
--constant <> 			: STD_LOGIC_VECTOR(0 to 5) := "011110";
--constant <> 			: STD_LOGIC_VECTOR(0 to 5) := "011111";
constant LB_OC	 		: STD_LOGIC_VECTOR(0 to 5) := "100000";
constant LH_OC		 	: STD_LOGIC_VECTOR(0 to 5) := "100001";
--constant <>			: STD_LOGIC_VECTOR(0 to 5) := "100010";
constant LW_OC			: STD_LOGIC_VECTOR(0 to 5) := "100011";
constant LBU_OC 		: STD_LOGIC_VECTOR(0 to 5) := "100100";
constant LHU_OC 		: STD_LOGIC_VECTOR(0 to 5) := "100101";
constant LF_OC 		: STD_LOGIC_VECTOR(0 to 5) := "100110";
constant LD_OC			: STD_LOGIC_VECTOR(0 to 5) := "100111";
constant SB_OC 		: STD_LOGIC_VECTOR(0 to 5) := "101000";
constant SH_OC 		: STD_LOGIC_VECTOR(0 to 5) := "101001";
--constant <>			: STD_LOGIC_VECTOR(0 to 5) := "101010";
constant SW_OC			: STD_LOGIC_VECTOR(0 to 5) := "101011";
--constant <> 			: STD_LOGIC_VECTOR(0 to 5) := "101100";
--constant <> 			: STD_LOGIC_VECTOR(0 to 5) := "101101";
constant SF_OC 		: STD_LOGIC_VECTOR(0 to 5) := "101110";
constant SD_OC 		: STD_LOGIC_VECTOR(0 to 5) := "101111";



function is_j_type (signal I_OPCODE : in STD_LOGIC_VECTOR(0 to 5)) return boolean;

function is_r_type_1 (signal I_OPCODE : in STD_LOGIC_VECTOR(0 to 5)) return boolean;

function is_r_type_2 (signal I_OPCODE : in STD_LOGIC_VECTOR(0 to 5)) return boolean;

function get_type (signal I_OPCODE : in STD_LOGIC_VECTOR(0 to 5)) return T_INSTRUCTION_TYPE;

function is_followed_by_if (signal I_OUT : in T_OUT_STATE) return boolean;

function is_execute_immediate (signal I_OUT : in T_OUT_STATE) return boolean;


end output_control_package;



package body output_control_package is

function is_j_type (signal I_OPCODE : in STD_LOGIC_VECTOR(0 to 5)) return boolean is
begin
	return i_opcode = J_OC or I_OPCODE = JAL_OC or I_OPCODE = RFE_OC or I_OPCODE = TRAP_OC;
end function;

function is_r_type_1 (signal I_OPCODE : in STD_LOGIC_VECTOR(0 to 5)) return boolean is
begin
	return I_OPCODE = SPECIAL_OC;
end function;

function is_r_type_2 (signal I_OPCODE : in STD_LOGIC_VECTOR(0 to 5)) return boolean is
begin
	return I_OPCODE = FPARITH_OC;
end function;

function get_type (signal I_OPCODE : in STD_LOGIC_VECTOR(0 to 5)) return T_INSTRUCTION_TYPE is
begin
	if is_j_type(I_OPCODE) then
		return j;
	end if;
	if is_r_type_1(I_OPCODE) then
		return r_1;
	end if;
	if is_r_type_2(I_OPCODE) then
		return r_2;
	end if;
	return i;
end function;

function is_followed_by_if (signal I_OUT : in T_OUT_STATE) return boolean is
begin
	return WB_IMMEDIATE_S = I_OUT 
		or BRANCH_COMPLETION_S = I_OUT
		or INV_BRANCH_COMPLETION_S = I_OUT
		or JUMP_COMPLETION_S = I_OUT 
		or R_TYPE_WB_S = I_OUT
		or MEM_SW_S = I_OUT
		or WB_LW_S = I_OUT
		or R_FP_TYPE_WB_S = I_OUT;
end function;

function is_execute_immediate (signal I_OUT : in T_OUT_STATE) return boolean is
begin
	return EXECUTE_ADD_IMMEDIATE_S = I_OUT
		or EXECUTE_SUB_IMMEDIATE_S  = I_OUT
		or EXECUTE_AND_IMMEDIATE_S  = I_OUT
		or EXECUTE_OR_IMMEDIATE_S   = I_OUT
		or EXECUTE_XOR_IMMEDIATE_S  = I_OUT
		or EXECUTE_SLL_IMMEDIATE_S  = I_OUT
		or EXECUTE_SRL_IMMEDIATE_S  = I_OUT
		or EXECUTE_SRA_IMMEDIATE_S  = I_OUT;
end function;


 
end output_control_package;
