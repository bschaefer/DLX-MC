-------------------------------------------------------------------------------
-- DLX_MULTI_CYCLE (DLX-MC)
-- Copyright 2016
-- https://gitlab.com/bschaefer/DLX-MC
--
-- This file is part of DLX-MC.
-- 
-- DLX-MC is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- DLX-MC is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with DLX-MC.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------
-- Module: 						memoryTB
-- Original author: 			dgermann
-- Co-authors:					bschaefer
-- Last changed:				07.01.2016
-- Description:
--	Testbench for memory
--
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
 
entity memorytb is
end memorytb;
 
architecture behavior of memorytb is 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    component memory
    port(
			i_reset : in STD_LOGIC;
         i_write_enable : in  STD_LOGIC;
         i_read_data : in  STD_LOGIC;
         i_address : in  STD_LOGIC_VECTOR(0 to 31);
         i_write_data : in  STD_LOGIC_VECTOR(0 to 31);
         o_mem_data : out  STD_LOGIC_VECTOR(0 to 31)
        );
    end component;
    
	type T_RAM is array(0 to 31) of BIT_VECTOR(31 downto 0); 
	
   --Inputs
	signal reset : STD_LOGIC := '0';
   signal write_enable : STD_LOGIC := '0';
   signal read_data : STD_LOGIC := '0';
   signal address : STD_LOGIC_VECTOR(0 to 31) := (others => '0');
   signal write_data : STD_LOGIC_VECTOR(0 to 31) := (others => '0');

 	--Outputs
   signal mem_data : STD_LOGIC_VECTOR(0 to 31);


begin
 
	-- instantiate the unit under test (uut)
   uut: memory port map (
			 i_reset => reset,
          i_write_enable => write_enable,
          i_read_data => read_data,
          i_address => address,
          i_write_data => write_data,
          o_mem_data => mem_data
        );
 

   -- stimulus process
   stim_proc: process
		variable ram : T_RAM;
   begin
		reset <= '1';
		wait for 10 ns;
		reset <= '0';
		wait for 10 ns;
		
		for i in 0 to 31 loop
			address <= conv_std_logic_vector(i*4, 32);
			read_data <= '1';
			wait for 5 ns;
			ram(i) := to_bitvector(mem_data);
			read_data <= '0';
			wait for 5 ns;
		end loop;
		
		for i in 0 to 31 loop
			address <= conv_std_logic_vector(i*4, 32);
			read_data <= '1';
			wait for 5 ns;
			assert ram(i) = to_bitvector(mem_data) report "RAM enth�lt nicht erwartete Daten an Stelle " & integer'image(i) severity error;
			read_data <= '0';
			wait for 5 ns;
		end loop;
      

      assert false report "Ende der Testbench" severity failure;
   end process;

end;
