-------------------------------------------------------------------------------
-- DLX_MULTI_CYCLE (DLX-MC)
-- Copyright 2016
-- https://gitlab.com/bschaefer/DLX-MC
--
-- This file is part of DLX-MC.
-- 
-- DLX-MC is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- DLX-MC is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with DLX-MC.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------
-- Module: 						alu
-- Original author: 			jgreilich
-- Co-authors:					bschaefer
-- Last changed:				17.02.2016
-- Description:
--	ALU executes asynchron the Operation given from the Alu-Control (i_operation)
-- whith the operands i_op_a and i_op_b and the result is visible on o_result.
-- 
-- MSB		  LSB
-- ........0...31   < Input
-- 0...31.32...63   < Calculation
-- ........0...31   < Output
--     /\
--    Carry
--
--
--
--
--
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all; -- arithmetic functions with Signed or Unsigned values

library work;
use work.dlx_package.all;


entity alu is
	port(
		i_op_a :  in STD_LOGIC_VECTOR(0 to 31);
		i_op_b:  in STD_LOGIC_VECTOR(0 to 31);
		i_operation : in T_ALU_OPERATION;
		o_flags : out T_ALU_FLAGS;
		o_result : out STD_LOGIC_VECTOR(0 to 31)
	);

end alu;

architecture Behavioral of alu is

-- Wires as signed
signal w_s_op_a : signed(0 to 63);
signal w_s_op_b : signed(0 to 63);
signal w_s_result : signed(0 to 63);

-- Wires as unsigned
signal w_u_op_a : unsigned(0 to 63);
signal w_u_op_b : unsigned(0 to 63);
signal w_u_result : unsigned(0 to 63);

signal w_result: STD_LOGIC_VECTOR(0 to 31);


--signal w_res_sra : signed(0 to 31);
--signal w_res_sla : signed(0 to 31);
signal w_res_mult : signed(0 to 63);
signal w_res_multu : unsigned(0 to 63);

signal w_res_slt : signed(0 to 63);
constant SIGNED_ZERO : signed(0 to 63) 
	:= "0000000000000000000000000000000000000000000000000000000000000000";
constant SIGNED_ONE : signed(0 to 63)  
	:= "0000000000000000000000000000000000000000000000000000000000000001";

begin
-- Multiplex the Operations --------------------------------------------
MuxALU_signed :
with i_operation select  w_s_result <=
	w_s_op_a + w_s_op_b when add,
	w_s_op_a - w_s_op_b when subtract,
	w_res_mult when multiply_signed,
	w_res_slt when set_lt,
	-- [ . . . ]
	(others => '0') when others;
	
MuxALU_unsigned :
with i_operation select w_u_result <=
	w_u_op_a sll to_integer(w_u_op_b(59 to 63)) when shift_left_logic,
	w_u_op_a srl to_integer(w_u_op_b(59 to 63)) when shift_right_logic,
	w_u_op_a and w_u_op_b when and_logic,
	w_u_op_a or w_u_op_b when or_logic,
	w_u_op_a xor w_u_op_b when xor_logic,
	w_res_multu when multiply_unsigned,
	-- [ . . . ]
	(others => '0') when others;



-- OPERATIONS -----------------------------------------------------------

MULT:
w_res_mult <= w_s_op_a(32 to 63) * w_s_op_b(32 to 63);

MULTU:
w_res_multu <= w_u_op_a(32 to 63) * w_u_op_b(32 to 63);

SLT:
w_res_slt <= SIGNED_ONE when w_s_op_a < w_s_op_b else SIGNED_ZERO;

-- SIGNALKONVERSION ------------------------------------------------------

-- Konvert the input std_logic_vector(0 to 31) to 64-bit signed
Signalkonversion:
process(i_op_a, i_op_b, i_operation) is
begin
	w_s_op_a(0 to 31) <= (others => i_op_a(0)); -- fill empty space with sign
	w_s_op_b(0 to 31) <= (others => i_op_b(0)); -- fill empty space with sign
	w_u_op_a(0 to 31) <= (others => '0');
	w_u_op_b(0 to 31) <= (others => '0');
	
	konv:
	for i in 0 to 31 loop
		w_s_op_a(i+32) <= i_op_a(i);
		w_s_op_b(i+32) <= i_op_b(i);
		w_u_op_a(i+32) <= i_op_a(i);
		w_u_op_b(i+32) <= i_op_b(i);
	end loop;
end process;


-- convert the output 64-bit signed to 32-bit std_logic_vector
ausgabeResult:
process(w_u_result, w_s_result, i_operation) is
begin	
	if i_operation = add
	or i_operation = subtract
	or i_operation = multiply_signed 
	or i_operation = set_lt then
		-- signed operations
		w_result <= STD_LOGIC_VECTOR(w_s_result(32 to 63));
	elsif i_operation = undefined_op then
		w_result <= VECTOR_UNDEFINED;
	else
		-- unsigned operations
		w_result <= STD_LOGIC_VECTOR(w_u_result(32 to 63));
	end if;
end process;
	
ausgabeFlags:
process (w_result, w_u_result, w_s_result, i_operation) is
begin
	if w_result	= (0 to 31 => '0') then
		o_flags.zero <= '1';
	else
		o_flags.zero <= '0';
	end if;
	
finalOutput:
o_result <= w_result;
	
		
end process;


	


end Behavioral;