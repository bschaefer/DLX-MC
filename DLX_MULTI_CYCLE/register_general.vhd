-------------------------------------------------------------------------------
-- DLX_MULTI_CYCLE (DLX-MC)
-- Copyright 2016
-- https://gitlab.com/bschaefer/DLX-MC
--
-- This file is part of DLX-MC.
-- 
-- DLX-MC is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- DLX-MC is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with DLX-MC.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------
-- Module: 						register_general
-- Original author: 			jgreilich
-- Co-authors:					
-- Last changed:				30.06.2015
-- Description:
-- array of d-ff with enable
--
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity register_general is
	port(
		i_clk: 	in  STD_LOGIC;
		i_reset: in  STD_LOGIC;
		i_en: 	in  STD_LOGIC;
		i_data: 	in  STD_LOGIC_VECTOR(0 to 31);
		o_data: 	out STD_LOGIC_VECTOR(0 to 31)
	);
end register_general;

architecture Behavioral of register_general is

signal f_data : STD_LOGIC_VECTOR(0 to 31) := (others => '0');

begin

process(i_clk, i_reset) is
begin
	if(i_reset = '1') then
		f_data <= (others => '0');
	else	
		if rising_edge(i_clk) and ( rising_edge(i_en) or i_en = '1') then
			f_data <= i_data;
		end if;
	end if;
end process;

-- Output
o_data <= f_data;

end Behavioral;