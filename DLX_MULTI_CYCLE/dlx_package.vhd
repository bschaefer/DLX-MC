-------------------------------------------------------------------------------
-- DLX_MULTI_CYCLE (DLX-MC)
-- Copyright 2016
-- https://gitlab.com/bschaefer/DLX-MC
--
-- This file is part of DLX-MC.
-- 
-- DLX-MC is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- DLX-MC is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with DLX-MC.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------
-- Package: 					dlx_package
-- Original author: 			bschaefer
-- Co-authors:					ptschubij, jgreilich
-- Last changed:				17.02.2016
-- Description:
--	constants and types for the DLX_MULTI_CYCLE project
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

package dlx_package is


type T_ALU_OP is (add, subtract, multiply, divide,
						and_logic, or_logic, xor_logic,
						shift_left_logic, shift_right_arith, shift_right_logic, 
						r_type, fp_r_type);
type T_I_OR_D is (instruction, data);
type T_ALU_OPERATION is (undefined_op, add, subtract, multiply_signed , multiply_unsigned, divide,
								and_logic, or_logic, xor_logic,
								shift_left_logic, shift_right_arith, shift_right_logic,
								set_lt);
type T_PC_SOURCE is (alu_result, alu_out, jmp_adr, const);
type T_ALU_SRC_B is (reg_b, four, immediate, i_address);
type T_ALU_SRC_A is (reg_a, pc);
type T_REG_DST is (d,t);
type T_ALU_FLAGS is
	record
		zero: STD_LOGIC;
	end record;
type T_RAM is array(0 to 255) of BIT_VECTOR(31 downto 0);
type T_GP_REGISTERS is array (0 to 31) of STD_LOGIC_VECTOR(0 to 31);
type T_REGISTER_SELECTION is (integer_register, floating_register);
	
	
--fcodes
constant SLL_FC		: STD_LOGIC_VECTOR(0 to 5) := "000100"; -- 0x04
constant SRL_FC		: STD_LOGIC_VECTOR(0 to 5) := "000110"; -- 0x06
constant SRA_FC		: STD_LOGIC_VECTOR(0 to 5) := "000111"; -- 0x07
constant TRAP_FC		: STD_LOGIC_VECTOR(0 to 5) := "001100"; -- 0x0c
constant ADD_FC		: STD_LOGIC_VECTOR(0 to 5) := "100000"; -- 0x20
constant ADDU_FC		: STD_LOGIC_VECTOR(0 to 5) := "100001";
constant SUB_FC		: STD_LOGIC_VECTOR(0 to 5) := "100010";
constant SUBU_FC		: STD_LOGIC_VECTOR(0 to 5) := "100011";
constant AND_FC		: STD_LOGIC_VECTOR(0 to 5) := "100100";
constant OR_FC			: STD_LOGIC_VECTOR(0 to 5) := "100101";
constant XOR_FC		: STD_LOGIC_VECTOR(0 to 5) := "100110";
--constant <>			: STD_LOGIC_VECTOR(0 to 5) := "100111";
constant SEQ_FC		: STD_LOGIC_VECTOR(0 to 5) := "101000";
constant SNE_FC		: STD_LOGIC_VECTOR(0 to 5) := "101001";
constant SLT_FC		: STD_LOGIC_VECTOR(0 to 5) := "101010";
constant SGT_FC		: STD_LOGIC_VECTOR(0 to 5) := "101011";
constant SLE_FC		: STD_LOGIC_VECTOR(0 to 5) := "101100";
constant SGE_FC		: STD_LOGIC_VECTOR(0 to 5) := "101101";
--constant <>			: STD_LOGIC_VECTOR(0 to 5) := "101110";
--constant <>			: STD_LOGIC_VECTOR(0 to 5) := "101111";
constant MOVI2S_FC	: STD_LOGIC_VECTOR(0 to 5) := "110000";
constant MOVS2I_FC	: STD_LOGIC_VECTOR(0 to 5) := "110001";
constant MOVF_FC		: STD_LOGIC_VECTOR(0 to 5) := "110010";
constant MOVD_FC		: STD_LOGIC_VECTOR(0 to 5) := "110011";
constant MOVFP2I_FC	: STD_LOGIC_VECTOR(0 to 5) := "110100";
constant MOVI2FP_FC	: STD_LOGIC_VECTOR(0 to 5) := "110101";


-- floating point fcodes
constant ADDF_FP_FC		: STD_LOGIC_VECTOR(0 to 4) := "00000";
constant SUBF_FP_FC		: STD_LOGIC_VECTOR(0 to 4) := "00001";
constant MULTF_FP_FC		: STD_LOGIC_VECTOR(0 to 4) := "00010";
constant DIVF_FP_FC		: STD_LOGIC_VECTOR(0 to 4) := "00011";
constant ADDD_FP_FC		: STD_LOGIC_VECTOR(0 to 4) := "00100";
constant SUBD_FP_FC		: STD_LOGIC_VECTOR(0 to 4) := "00101";
constant MULTD_FP_FC		: STD_LOGIC_VECTOR(0 to 4) := "00110";
constant DIVD_FP_FC		: STD_LOGIC_VECTOR(0 to 4) := "00111";
constant CVTF2D_FP_FC	: STD_LOGIC_VECTOR(0 to 4) := "01000";
constant CVTF2I_FP_FC	: STD_LOGIC_VECTOR(0 to 4) := "01001";
constant CVTD2F_FP_FC	: STD_LOGIC_VECTOR(0 to 4) := "01010";
constant CVTD2I_FP_FC	: STD_LOGIC_VECTOR(0 to 4) := "01011";
constant CVTI2F_FP_FC	: STD_LOGIC_VECTOR(0 to 4) := "01100";
constant CVTI2D_FP_FC	: STD_LOGIC_VECTOR(0 to 4) := "01101";
constant MULT_FP_FC		: STD_LOGIC_VECTOR(0 to 4) := "01110";
constant DIV_FP_FC		: STD_LOGIC_VECTOR(0 to 4) := "01111";
constant EQF_FP_FC		: STD_LOGIC_VECTOR(0 to 4) := "10000";
constant NEF_FP_FC		: STD_LOGIC_VECTOR(0 to 4) := "10001";
constant LTF_FP_FC		: STD_LOGIC_VECTOR(0 to 4) := "10010";
constant GTF_FP_FC		: STD_LOGIC_VECTOR(0 to 4) := "10011";
constant LEF_FP_FC		: STD_LOGIC_VECTOR(0 to 4) := "10100";
constant GEF_FP_FC		: STD_LOGIC_VECTOR(0 to 4) := "10101";
constant MULTU_FP_FC		: STD_LOGIC_VECTOR(0 to 4) := "10110";
constant DIVU_FP_FC		: STD_LOGIC_VECTOR(0 to 4) := "10111";
constant EQD_FP_FC		: STD_LOGIC_VECTOR(0 to 4) := "11000";
constant NEQ_FP_FC		: STD_LOGIC_VECTOR(0 to 4) := "11001";
constant LTD_FP_FC		: STD_LOGIC_VECTOR(0 to 4) := "11010";
constant GTD_FP_FC		: STD_LOGIC_VECTOR(0 to 4) := "11011";
constant LED_FP_FC		: STD_LOGIC_VECTOR(0 to 4) := "11100";
constant GED_FP_FC		: STD_LOGIC_VECTOR(0 to 4) := "11101";


-- other constants
constant VECTOR_FOUR: 		STD_LOGIC_VECTOR(0 to 31) :=
	"00000000000000000000000000000100";
constant VECTOR_ZERO: 		STD_LOGIC_VECTOR(0 to 31) :=
	"00000000000000000000000000000000";
constant VECTOR_UNDEFINED: STD_LOGIC_VECTOR(0 to 31) :=
	"UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU";

end dlx_package;

package body dlx_package is


 
end dlx_package;