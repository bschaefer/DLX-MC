-------------------------------------------------------------------------------
-- DLX_MULTI_CYCLE (DLX-MC)
-- Copyright 2016
-- https://gitlab.com/bschaefer/DLX-MC
--
-- This file is part of DLX-MC.
-- 
-- DLX-MC is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- DLX-MC is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with DLX-MC.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------
-- Module: 						register_array_testing
-- Original author: 			jgreilich (register_array)
-- Co-authors:					bschaefer
-- Last changed:				07.12.2015
-- Description:
--	Array of multi purpose registers
-- extended output for testing
--
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

library work;
use work.dlx_package.all;

entity register_array_testing is
	port(
		i_clk				  : in STD_LOGIC;
		i_reset			  : in STD_LOGIC;
		i_read_register1 : in STD_LOGIC_VECTOR(0 to 4);
		i_read_register2 : in STD_LOGIC_VECTOR(0 to 4);
		i_write_register : in STD_LOGIC_VECTOR(0 to 4);
		i_write_data	  : in STD_LOGIC_VECTOR(0 to 31);
		i_reg_write		  : in STD_LOGIC;
		o_read_data1	  : out STD_LOGIC_VECTOR(0 to 31);
		o_read_data2	  : out STD_LOGIC_VECTOR(0 to 31);
		o_registers		  : out T_GP_REGISTERS
	);	
end register_array_testing;

architecture Behavioral of register_array_testing is

component register_general is
	port(
		i_clk: 	in  STD_LOGIC;
		i_reset: in  STD_LOGIC;
		i_en: 	in  STD_LOGIC;
		i_data: 	in  STD_LOGIC_VECTOR(0 to 31);
		o_data: 	out STD_LOGIC_VECTOR(0 to 31)
	);
end component register_general;

signal w_data_out : T_GP_REGISTERS;
signal w_en : STD_LOGIC_VECTOR(0 to 31);

begin

Reg0:
w_data_out(0) <= (others => '0');

Reg1to31:
for i in 1 to 31 generate
	regn : register_general
	port map(
		i_clk => i_clk,
		i_reset => i_reset,
		i_en => w_en(i),
		i_data => i_write_data,
		o_data => w_data_out(i)
	);
end generate;

WriteEnable:
process (i_reg_write, i_write_register) is
begin
	w_en <= (others => '0');
	if(i_reg_write = '1') then
		w_en(conv_integer(i_write_register)) <= '1';
	end if;
end process;

-- Ouputs
o_read_data1 <= w_data_out(conv_integer(i_read_register1));
o_read_data2 <= w_data_out(conv_integer(i_read_register2));

testing_output:
o_registers <= w_data_out;

end Behavioral;