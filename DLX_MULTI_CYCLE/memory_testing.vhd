-------------------------------------------------------------------------------
-- DLX_MULTI_CYCLE (DLX-MC)
-- Copyright 2016
-- https://gitlab.com/bschaefer/DLX-MC
--
-- This file is part of DLX-MC.
-- 
-- DLX-MC is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- DLX-MC is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with DLX-MC.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------
-- Module: 						memory_testing
-- Original author: 			dgermann (memory)
-- Co-authors:					bschaefer
-- Last changed:				08.12.2015
-- Description:
--	32bit-word, byte addressed, random access memory
-- initialized from file given by generic parameter INITIALISATION
-- extended entity for testing
--
-------------------------------------------------------------------------------
library ieee;
use std.textio.all;
use ieee.std_logic_1164.all;
use ieee.std_logic_textio.all;
use ieee.std_logic_unsigned.all;

library work;
use work.dlx_package.all;

entity memory_testing is
	generic(INITIALISATION: STRING);
	port(
		i_reset:			 in STD_LOGIC;
		i_write_enable: in STD_LOGIC;
		i_read_data :   in STD_LOGIC;
		i_address: 		 in STD_LOGIC_VECTOR(0 to 31);
		i_write_data: 	 in STD_LOGIC_VECTOR(0 to 31);
		o_mem_data: 	 out STD_LOGIC_VECTOR(0 to 31);
		o_ram:			 out T_RAM
	);	
end memory_testing;


architecture Behavioral of memory_testing is
 
 function string_line_to_std_logic_vector (line : in STRING) return STD_LOGIC_VECTOR is
		variable char : CHARACTER := NUL;
		variable vector : STD_LOGIC_VECTOR(0 to 31);
		variable pos : INTEGER := 0;
		variable symbol : STD_LOGIC_VECTOR(0 to 3);
	begin
		for i in line'range loop
			char := line(i);
			if char = ';' then
					return VECTOR_UNDEFINED; --invalid
			end if;
			
			case char is
				when '0' => symbol := "0000";
				when '1' => symbol := "0001";
				when '2' => symbol := "0010";
				when '3' => symbol := "0011";
				when '4' => symbol := "0100";
				when '5' => symbol := "0101";
				when '6' => symbol := "0110";
				when '7' => symbol := "0111";
				when '8' => symbol := "1000";
				when '9' => symbol := "1001";
				when 'A' | 'a' => symbol := "1010";
				when 'B' | 'b' => symbol := "1011";
				when 'C' | 'c' => symbol := "1100";
				when 'D' | 'd' => symbol := "1101";
				when 'E' | 'e' => symbol := "1110";
				when 'F' | 'f' => symbol := "1111";
				when others => symbol := "UUUU";
			end case;
			
			vector(pos to pos+3) := symbol;
			pos := pos + 4;
			
			if pos = 32 then
				return vector;
			end if;
			
		end loop;
		
		return VECTOR_UNDEFINED;
	
	end function;
 
 impure function read_ram (ram_source: in STRING) return T_RAM is                                                   
    FILE ram_source_file         : TEXT is in ram_source;                       
    variable read_line  : LINE;                                 
    variable ram        : T_RAM;
	 variable sline	   : STRING(1 to 8);
	 variable hlp_vector : STD_LOGIC_VECTOR(0 to 31);
 begin
    for i in T_RAM'range loop
			comment_loop:
			while not endfile(ram_source_file) loop 
			  readline (ram_source_file, read_line);
			  sline := (others => NUL);
			  read (read_line, sline);
			  hlp_vector := string_line_to_std_logic_vector(sline);		  
			  
			  next comment_loop when hlp_vector = VECTOR_UNDEFINED;

			  ram(i) := to_bitvector(hlp_vector);	  
			  exit;	  
			end loop;
			
			if endfile(ram_source_file) then
				ram(i) := to_bitvector(VECTOR_ZERO);
			end if;
    end loop;                                                    
    return ram;                                                  
 end function; 

 signal ram : T_RAM := read_ram(INITIALISATION);



begin
	process (i_reset, i_write_enable, i_read_data, i_address)                                                
		begin                                                        
			if i_reset = '1' then
				ram <= read_ram(INITIALISATION);
			else
				if i_write_enable = '1' then
					if conv_integer(i_address) mod 4 = 0 then -- word address
						ram(conv_integer(i_address)/4) <= to_bitvector(i_write_data);
					elsif conv_integer(i_address) mod 2 = 0 then -- half word address
						-- TODO for sh
						-- synthesis off
						report "Invalid halfword address " & INTEGER'image(conv_integer(i_address));
						-- synthesis on
					else -- byte address
						-- TODO for sb
						-- synthesis off
						report "Invalid byte address " & INTEGER'image(conv_integer(i_address));
						-- synthesis on
					end if;				
					         
				end if;
				
				if i_read_data = '1' then
					if conv_integer(i_address) mod 4 = 0 then -- word address
						o_mem_data <= to_stdlogicvector(ram(conv_integer(i_address)/4));
					elsif conv_integer(i_address) mod 2 = 0 then -- half word address
						o_mem_data <= VECTOR_UNDEFINED;
						-- TODO for lh
						-- synthesis off
						report "Invalid halfword address " & INTEGER'image(conv_integer(i_address));
						-- synthesis on
					else -- byte address
						o_mem_data <= VECTOR_UNDEFINED;
						-- TODO for lb
						-- synthesis off
						report "Invalid byte address " & INTEGER'image(conv_integer(i_address));
						-- synthesis on
					end if;
				end if;	   
			end if;
	end process;
	
	testing_output:
	o_ram <= ram;

end Behavioral;