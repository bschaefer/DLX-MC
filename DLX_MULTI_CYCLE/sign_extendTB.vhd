-------------------------------------------------------------------------------
-- Modul: 						sign_extend
-- Hauptveranwortlicher: 	dgermann
-- Letzte Anderung:			12.05.2015
-- Beschreibung:
--	<beschreibung>
--
-------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
 
ENTITY sign_extendTB IS
END sign_extendTB;
 
ARCHITECTURE behavior OF sign_extendTB IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT sign_extend
    PORT(
         i_instruction_16 : IN  std_logic_vector(0 to 15);
         o_instruction_32 : OUT  std_logic_vector(0 to 31)
        );
    END COMPONENT;
    

   --Inputs
   signal i_instruction_16 : std_logic_vector(0 to 15) := (others => '0');

 	--Outputs
   signal o_instruction_32 : std_logic_vector(0 to 31);
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: sign_extend PORT MAP (
          i_instruction_16 => i_instruction_16,
          o_instruction_32 => o_instruction_32
        );


   -- Stimulus process
   stim_proc: process
   begin		
		report " Beginn der Testbench ";
		i_instruction_16  <= (others => '1');
		wait for 10 ns;
		assert(o_instruction_32 = "11111111111111111111111111111111") report" o_instruction ist nicht o_data=11111111111111111111111111111111"severity failure;
		report " Ende der Testbench "severity failure;
   end process;

END;
