-------------------------------------------------------------------------------
-- DLX_MULTI_CYCLE (DLX-MC)
-- Copyright 2016
-- https://gitlab.com/bschaefer/DLX-MC
--
-- This file is part of DLX-MC.
-- 
-- DLX-MC is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- DLX-MC is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with DLX-MC.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------
-- Module: 						verifying_systemTB
-- Original author: 			bschaefer
-- Co-authors:
-- Last changed:				17.02.2016
-- Description:
--	A testbench for a modified version of the dlx_processor. This version allows
--	automated testing.
--
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
 
library work;
use work.dlx_package.all;
 
entity verifying_systemtb is
end verifying_systemtb;
 
architecture behavior of verifying_systemtb is 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    component dlx_processor_testing
	 generic(INITIALISATION: STRING);
    port(
         i_clk: 			in  STD_LOGIC;
         i_reset: 		in  STD_LOGIC;
         o_ram: 			out T_RAM;
			o_registers:	out T_GP_REGISTERS;
			o_fp_registers:out T_GP_REGISTERS;
			o_pc:				out STD_LOGIC_VECTOR(0 to 31);
			o_state_nr: 	out INTEGER
        );
    end component;
	 
	 function std_logic_vector_to_string ( vector : STD_LOGIC_VECTOR ) return STRING is
		variable out_string : STRING(1 to 32);
		variable j : INTEGER := 1;
	 begin
		for i in vector'RANGE loop
			out_string(j) := STD_LOGIC'IMAGE(vector(i))(2); -- IMAGE returns results like '1', '0', ...
			j := j + 1;
		end loop;
		return out_string;
		
	 end function;
	 
	 
	 function assert_equal ( 	operand_one : STD_LOGIC_VECTOR(0 to 31);
										operand_two : STD_LOGIC_VECTOR(0 to 31);
										instruction : STD_LOGIC_VECTOR(0 to 31) )
									return INTEGER
	 is
	 begin
		report "asserting instruction " & INTEGER'IMAGE(CONV_INTEGER(instruction)/4 - 1);
		assert operand_one = operand_two 
			report std_logic_vector_to_string(operand_one) 
					& " not equal to " 
					& std_logic_vector_to_string(operand_two)
					& " at instruction "
					& INTEGER'IMAGE(CONV_INTEGER(instruction)/4 - 1)
			severity error;
			return 0;
	 end function assert_equal;
	 
	 function assert_equal ( 	operand_one : STD_LOGIC_VECTOR(0 to 31);
										operand_two : INTEGER;
										instruction : STD_LOGIC_VECTOR(0 to 31) )
									return INTEGER
	 is
	 begin
		return assert_equal( operand_one, CONV_STD_LOGIC_VECTOR(operand_two, 32), instruction);
	 end function assert_equal;
	 
	 function assert_equal ( 	operand_one : BIT_VECTOR(0 to 31);
										operand_two : INTEGER;
										instruction : STD_LOGIC_VECTOR(0 to 31) )
									return INTEGER
	 is
	 begin
		return assert_equal( TO_STDLOGICVECTOR(operand_one), CONV_STD_LOGIC_VECTOR(operand_two, 32), instruction);
	 end function assert_equal;
	 
	 function assert_not_equal	(	operand_one : STD_LOGIC_VECTOR(0 to 31);
											operand_two : STD_LOGIC_VECTOR(0 to 31);
											instruction : STD_LOGIC_VECTOR(0 to 31) )
									return INTEGER
	 is
	 begin
		assert operand_one /= operand_two 
			report std_logic_vector_to_string(operand_one) 
					& " not equal to " 
					& std_logic_vector_to_string(operand_two)
					& " at instruction "
					& INTEGER'IMAGE(CONV_INTEGER(instruction)/4 - 1)
			severity error;
			return 0;
	 end function assert_not_equal;
	 
	 function assert_not_equal ( 	operand_one : STD_LOGIC_VECTOR(0 to 31);
											operand_two : INTEGER;
											instruction : STD_LOGIC_VECTOR(0 to 31) )
									return INTEGER
	 is
	 begin
		return assert_not_equal( operand_one, CONV_STD_LOGIC_VECTOR(operand_two, 32), instruction);
	 end function assert_not_equal;
    

   --Inputs
   signal clk : STD_LOGIC := '0';
   signal reset : STD_LOGIC := '0';

 	--Outputs
   signal ram : T_RAM;
	signal registers : T_GP_REGISTERS;
	signal fp_registers : T_GP_REGISTERS;
	signal pc : STD_LOGIC_VECTOR(0 to 31);
	signal state_nr : INTEGER;

   -- Clock period definitions
   constant CLK_PERIOD : TIME := 10 ns;
 
begin
 
	-- Instantiate the Unit Under Test (UUT)
   uut: dlx_processor_testing
		generic map(INITIALISATION => "examples/verifying_systemTB.ram")
		port map (
          i_clk => clk,
          i_reset => reset,
          o_ram => ram,
			 o_registers => registers,
			 o_fp_registers => fp_registers,
			 o_pc => pc,
			 o_state_nr => state_nr
      );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for CLK_PERIOD/2;
		clk <= '1';
		wait for CLK_PERIOD/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
		variable ret_int : INTEGER;
   begin
		wait until state_nr = 1; -- wait for 1st instruction
		-- j init
		wait until state_nr = 1;
		
		-- addi r1, r0, -42
		-- init:
		-- addi r0, r0, 666
		wait until state_nr = 1;
		ret_int := assert_equal( registers(0), 0, pc);
		-- addi r1, r0, 42
		wait until state_nr = 1;
		ret_int := assert_equal( registers(1), 42, pc);
		
		-- addi r2, r0, 12
		wait until state_nr = 1;
		ret_int := assert_equal( registers(2), 12, pc);
	
		-- add r3, r2, r0
		wait until state_nr = 1;
		ret_int := assert_equal( registers(3), 12, pc);
		
		-- add r3, r2, r1
		wait until state_nr = 1;
		ret_int := assert_equal( registers(3), 54, pc);
		
		-- subi r4, r1, 40
		wait until state_nr = 1;
		ret_int := assert_equal( registers(4), 2, pc);
		
		-- subi r4, r2, 12
		wait until state_nr = 1;
		ret_int := assert_equal( registers(4), 0, pc);
		
		-- sub r4, r3, r1
		wait until state_nr = 1;
		ret_int := assert_equal( registers(4), 12, pc);
		
		-- sub r4, r4, r2
		wait until state_nr = 1;
		ret_int := assert_equal( registers(4), 0, pc);
		
		-- beqz r4, ok_1
		wait until state_nr = 1;
		-- j error
		-- ok_1:
		-- bnez r3, ok_2
		wait until state_nr = 1;
		-- j error
		-- ok_2:
		-- beqz r1, error
		wait until state_nr = 1;
		-- bnez r8, error
		wait until state_nr = 1;
		-- j logical
		wait until state_nr = 1;
		-- error:
		-- addi r1, r0, 666
		-- logical:
		-- and r4, r1, r2
		wait until state_nr = 1;
		ret_int := assert_equal( registers(4), 8, pc);
		ret_int := assert_not_equal( registers(1), 666, pc);
		
		-- or r5, r1, r2
		wait until state_nr = 1;
		ret_int := assert_equal( registers(5), 46, pc);
		
		-- xor r6, r1, r2
		wait until state_nr = 1;
		ret_int := assert_equal( registers(6), 38, pc);
		
		-- andi r4, r4, 42
		wait until state_nr = 1;
		ret_int := assert_equal( registers(4), 8, pc);
		
		-- ori r5, r5, 1
		wait until state_nr = 1;
		ret_int := assert_equal( registers(5), 47, pc);
		
		-- xori r6, r6, 39
		wait until state_nr = 1;
		ret_int := assert_equal( registers(6), 1, pc);
		
		-- sll r5, r5, r6
		wait until state_nr = 1;
		ret_int := assert_equal( registers(5), 94, pc);
		
		-- srli r5, r5, 1
		wait until state_nr = 1;
		ret_int := assert_equal( registers(5), 47, pc);
		
		-- srl r4, r4, r6
		wait until state_nr = 1;
		ret_int := assert_equal( registers(4), 4, pc);
		
		-- slli r4, r4, 1
		wait until state_nr = 1;
		ret_int := assert_equal( registers(4), 8, pc);
		
		-- sw 32(r0), r1
		wait until state_nr = 1;
		ret_int := assert_equal( ram(8), 42, pc);
		
		-- lw r7, 32(r0)
		wait until state_nr = 1;
		ret_int := assert_equal( registers(7), 42, pc);
		
		-- movi2fp r1, r4
		wait until state_nr = 1;
		ret_int := assert_equal( fp_registers(1), 8, pc);
		
		-- movi2fp r2, r7 
		wait until state_nr = 1;
		ret_int := assert_equal( fp_registers(2), 42, pc);
		
		-- mult r3, r1, r2
		wait until state_nr = 1;
		ret_int := assert_equal( fp_registers(3), 336, pc);
		
		-- movfp2i r8, r3
		wait until state_nr = 1;
		ret_int := assert_equal( registers(8), 336, pc);
		
		-- r8 = 7
		wait until state_nr = 1;
		ret_int := assert_equal( registers(8), 7, pc);
		
		-- r9 = 103
		wait until state_nr = 1;
		ret_int := assert_equal( registers(9), 103, pc);
		
		-- r10 = 1
		wait until state_nr = 1;
		ret_int := assert_equal( registers(10), 1, pc);
		
		-- r10 = 0
		wait until state_nr = 1;
		ret_int := assert_equal( registers(10), 0, pc);
		
		-- r10 = 1
		wait until state_nr = 1;
		ret_int := assert_equal( registers(10), 1, pc);
		
		-- r8 = 103
		wait until state_nr = 1;
		ret_int := assert_equal( registers(8), 103, pc);
		
		-- r10 = 0
		wait until state_nr = 1;
		ret_int := assert_equal( registers(10), 0, pc);
		
		-- r8 = -1022
		wait until state_nr = 1;
		ret_int := assert_equal( registers(8), -1022, pc);
		
		-- r10 = 1
		wait until state_nr = 1;
		ret_int := assert_equal( registers(10), 1, pc);
		
		-- r10 = 0
		wait until state_nr = 1;
		ret_int := assert_equal( registers(10), 0, pc);
		
		report "End of testbench" severity failure;
		wait;
   end process;

end;
