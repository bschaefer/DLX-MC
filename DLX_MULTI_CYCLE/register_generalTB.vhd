-------------------------------------------------------------------------------
-- DLX_MULTI_CYCLE (DLX-MC)
-- Copyright 2016
-- https://gitlab.com/bschaefer/DLX-MC
--
-- This file is part of DLX-MC.
-- 
-- DLX-MC is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- DLX-MC is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with DLX-MC.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------
-- Module: 						register_generalTB
-- Original author: 			jgreilich
-- Co-authors:					
-- Last changed:				07.01.2016
-- Description:
-- test of array of d-ff with enable
--
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
 
 
entity register_generaltb is
end register_generaltb;
 
architecture behavior of register_generaltb is 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    component register_general
    port(
         i_reset : in  STD_LOGIC;
         i_clk : in  STD_LOGIC;
         i_en : in  STD_LOGIC;
         i_data : in  STD_LOGIC_VECTOR(0 to 31);
         o_data : out  STD_LOGIC_VECTOR(0 to 31)
        );
    end component;
    

   --Inputs
   signal i_reset : STD_LOGIC := '0';
   signal i_clk : STD_LOGIC := '0';
   signal i_en : STD_LOGIC := '0';
   signal i_data : STD_LOGIC_VECTOR(0 to 31) := (others => '0');

 	--Outputs
   signal o_data : STD_LOGIC_VECTOR(0 to 31);

   -- Clock period definitions
   constant I_CLK_PERIOD : TIME := 10 ns;
 
begin
 
	-- Instantiate the Unit Under Test (UUT)
   uut: register_general port map (
          i_reset => i_reset,
          i_clk => i_clk,
          i_en => i_en,
          i_data => i_data,
          o_data => o_data
        );

   -- Clock process definitions
   i_clk_process :process
   begin
		i_clk <= '0';
		wait for I_CLK_PERIOD/2;
		i_clk <= '1';
		wait for I_CLK_PERIOD/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   
	variable value : STD_LOGIC_VECTOR(0 to 31) := (0 => '1', 10 => '1', others => '0');
	begin		
      
		-- Initialisierung der Eing�nge und Reset
		i_en <= '0';
		i_data <= (others => '0');
		i_reset <= '1';
		wait for 12 ns; -- 2ns nach fallender, 3ns vor steigender Taktflanke
		
		i_reset <= '0';
		

		-- Eigabedaten setzen
		i_data <= value;
		i_en <= '1';
      
		wait for 5 ns; -- 2ns nach steigender Taktflanke
		
		-- Ausgabedaten pr�fen
		
		
		assert o_data = value report "Value ist falsch!" severity error;
			
		wait for 5 ns; -- 2ns nach fallender Taktflanke
		
		
		
		assert false report "Ende der Testbench." severity failure;
      wait;
   end process;

END;
