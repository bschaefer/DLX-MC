-------------------------------------------------------------------------------
-- DLX_MULTI_CYCLE (DLX-MC)
-- Copyright 2016
-- https://gitlab.com/bschaefer/DLX-MC
--
-- This file is part of DLX-MC.
-- 
-- DLX-MC is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- DLX-MC is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with DLX-MC.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------
-- Module: 						register_arrayTB
-- Original author: 			jgreilich
-- Co-authors:					
-- Last changed:				07.01.2016
-- Description:
-- write and read registers 1 to 31 once for data1 and once for data2
-- read register 0, try write and read again
--
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
 

 
entity register_arraytb is
end register_arraytb;
 
architecture behavior of register_arraytb is 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    component register_array
    port(
         i_reset : in STD_LOGIC;
			i_clk : in  STD_LOGIC;
         i_read_register1 : in  STD_LOGIC_VECTOR(0 to 4);
         i_read_register2 : in  STD_LOGIC_VECTOR(0 to 4);
         i_write_register : in  STD_LOGIC_VECTOR(0 to 4);
         i_write_data : in  STD_LOGIC_VECTOR(0 to 31);
         i_reg_write : in  STD_LOGIC;
         o_read_data1 : out  STD_LOGIC_VECTOR(0 to 31);
         o_read_data2 : out  STD_LOGIC_VECTOR(0 to 31)
        );
    end component;
    

   --Inputs
	
   signal i_clk : STD_LOGIC := '0';
   signal i_read_register1 : STD_LOGIC_VECTOR(0 to 4) := (others => '0');
   signal i_read_register2 : STD_LOGIC_VECTOR(0 to 4) := (others => '0');
   signal i_write_register : STD_LOGIC_VECTOR(0 to 4) := (others => '0');
   signal i_write_data : STD_LOGIC_VECTOR(0 to 31) := (others => '0');
   signal i_reg_write : STD_LOGIC := '0';
	signal i_reset : STD_LOGIC := '1';


 	--Outputs
   signal o_read_data1 : STD_LOGIC_VECTOR(0 to 31);
   signal o_read_data2 : STD_LOGIC_VECTOR(0 to 31);

   -- Clock period definitions
   constant I_CLK_PERIOD : TIME := 10 ns;
   
   type signalarray is array (0 to 31) of STD_LOGIC_VECTOR(0 to 31);
   shared variable array_sigs_check : SIGNALARRAY;
   shared variable value : STD_LOGIC_VECTOR(0 to 31);
	shared variable j : INTEGER;


begin
  
 
	-- Instantiate the Unit Under Test (UUT)
   uut: register_array port map (
          i_reset => i_reset,
			 i_clk => i_clk,
          i_read_register1 => i_read_register1,
          i_read_register2 => i_read_register2,
          i_write_register => i_write_register,
          i_write_data => i_write_data,
          i_reg_write => i_reg_write,
          o_read_data1 => o_read_data1,
          o_read_data2 => o_read_data2
        );

   -- Clock process definitions
   i_clk_process :process
   begin
		i_clk <= '0';
		wait for I_CLK_PERIOD/2;
		i_clk <= '1';
		wait for I_CLK_PERIOD/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin
	
		report "Beginn der Testbench.------------------------------------------------------";
	
		-- Signale initialisieren
		i_read_register1 <= (others => '0');
		i_read_register2 <= (others => '0');
		i_write_register <= (others => '0');
		i_write_data <= (others => '0');
		i_reg_write <= '0';
		i_reset <= '1';
	
	
		-- Reset
		
		wait for i_clk_period;
		i_reset <= '0';
		
		
		
		-- Register 1-31 bef�llen
		
		i_reg_write <= '1';
		
		for i in 1 to 31 loop -- eigentlich bis 31
			
			wait for 2 ns;
			
			value := conv_std_logic_vector(37,32); -- <-- hier eventuell Zufallszahl generieren
			array_sigs_check(i) := value;
			i_write_data <= value;
			i_write_register <= conv_std_logic_vector(i,5);
			
			wait for 8 ns;
			
			
		end loop;
		
		
		-- Register 1-31 auslesen
		
		i_reg_write <= '0';
		 
		for i in 1 to 31 loop
			
			wait for 2 ns;
			
			j := 32-i;
			
			i_read_register1 <= conv_std_logic_vector(i,5);
			i_read_register2 <= conv_std_logic_vector(j,5);
			
			wait for 8 ns;
			
			assert array_sigs_check(i) = o_read_data1 report "Data1: Register " & INTEGER'image(i) & " falsch." severity failure;
			assert array_sigs_check(j) = o_read_data2 report "Data2: Register " & INTEGER'image(j) & " falsch." severity failure;

		end loop;
		 
		
		-- Register 0 lesen
		
		wait for 2 ns;
			
			i_read_register1 <= conv_std_logic_vector(100,5);
			i_read_register2 <= conv_std_logic_vector(j,5);
			
			wait for 8 ns;
			
			-- assert 0 = o_read_data1 report "Data1: Register " & integer'image(i) & " falsch." severity failure;
		
		
		-- Schreibversuch Register 0
		
		
		
		-- Register 0 lesen nach Schreibversuch
		
		
		
		
		
		assert false report "Ende der Testbench. -------------------------------------------------------" severity failure;
		
	  
	  
	  
   end process;

end;
