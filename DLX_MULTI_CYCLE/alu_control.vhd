-------------------------------------------------------------------------------
-- DLX_MULTI_CYCLE (DLX-MC)
-- Copyright 2016
-- https://gitlab.com/bschaefer/DLX-MC
--
-- This file is part of DLX-MC.
-- 
-- DLX-MC is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- DLX-MC is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with DLX-MC.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------
-- Module: 						alu_control
-- Original author: 			jgreilich
-- Co-authors:					bschaefer
-- Last changed:				17.02.2016
-- Description:
--	Alu-Control is asynchrone and chooses the Operation, which the ALU has to 
-- execute. This depends on the Operation, which the Output-Control sends to 
-- Alu-Control (i_alu_op). When the Operation by the Output-Control is "R-Type",
-- Alu-Control chooses the Operation by the Functions-Code (i_instruction) 
--
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
library work;
use work.dlx_package.all;

entity alu_control is
	port(
		i_instruction: 			in STD_LOGIC_VECTOR(0 to 5);
		i_alu_op: 					in T_ALU_OP;
		o_operation: 				out T_ALU_OPERATION;
		o_register_source: 		out T_REGISTER_SELECTION;
		o_register_destination:	out T_REGISTER_SELECTION
	);

end alu_control;

architecture Behavioral of alu_control is

signal w_rtype_operation : T_ALU_OPERATION;
signal w_fp_instruction : STD_LOGIC_VECTOR(0 to 4);


begin


output_mux:
process(i_instruction, i_alu_op, w_rtype_operation) is
begin
	case i_alu_op is
		when add => o_operation <= add;
		when subtract => o_operation <= subtract;
		when shift_left_logic => o_operation <= shift_left_logic;
		when shift_right_logic => o_operation <= shift_right_logic;
		when and_logic => o_operation <= and_logic;
		when or_logic => o_operation <= or_logic;
		when xor_logic => o_operation <= xor_logic;
		when r_type => o_operation <= w_rtype_operation;
		when fp_r_type => o_operation <= w_rtype_operation;
		when others => o_operation <= undefined_op;
	end case;

end process;


rtype_operation:
process (i_instruction, i_alu_op) is
begin
	if i_alu_op = r_type then
		case i_instruction is
			when ADD_FC 		=> w_rtype_operation <= add;
			when SUB_FC 		=> w_rtype_operation <= subtract;
			when SLL_FC 		=> w_rtype_operation <= shift_left_logic;
			when SRL_FC 		=> w_rtype_operation <= shift_right_logic;
			when SRA_FC 		=> w_rtype_operation <= shift_right_arith;
			when AND_FC 		=> w_rtype_operation <= and_logic;
			when OR_FC 			=> w_rtype_operation <= or_logic;
			when XOR_FC 		=> w_rtype_operation <= xor_logic;
			when MOVI2FP_FC	=> w_rtype_operation <= add;
			when MOVFP2I_FC	=> w_rtype_operation <= add;
			when SLT_FC			=> w_rtype_operation <= set_lt;
			
			-- default
			when others => w_rtype_operation <= undefined_op;
		end case;
		if i_instruction = ADD_FC then
			w_rtype_operation <= add;
		end if;
	elsif i_alu_op = fp_r_type then
		case w_fp_instruction is
			when MULT_FP_FC 	=> w_rtype_operation <= multiply_signed;
			when MULTU_FP_FC 	=> w_rtype_operation <= multiply_unsigned;
			when others			=> w_rtype_operation <= undefined_op; --not correct for fp registers
		end case;
	else
		w_rtype_operation <= undefined_op;
	end if;
end process;
	
	
fp_instruction:
w_fp_instruction <= i_instruction(1 to 5);


register_selection:
process (i_instruction, i_alu_op) is
begin
	if i_alu_op = fp_r_type then
		o_register_source <= floating_register;
		o_register_destination <= floating_register;
	elsif i_alu_op = r_type then
		if i_instruction = MOVI2FP_FC then
			o_register_source <= integer_register;
			o_register_destination <= floating_register;
		elsif i_instruction = MOVFP2I_FC then
			o_register_source <= floating_register;
			o_register_destination <= integer_register;
		else		
			o_register_source <= integer_register;
			o_register_destination <= integer_register;
		end if;
	else
		o_register_source <= integer_register;
		o_register_destination <= integer_register;
	end if;
end process;


end Behavioral;