-------------------------------------------------------------------------------
-- DLX_MULTI_CYCLE (DLX-MC)
-- Copyright 2016
-- https://gitlab.com/bschaefer/DLX-MC
--
-- This file is part of DLX-MC.
-- 
-- DLX-MC is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- DLX-MC is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with DLX-MC.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------
-- Module: 						register_instructionTB
-- Original author: 			jgreilich
-- Co-authors:					
-- Last changed:				07.01.2016
-- Description:
-- test of instruction_register
--
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

library work;
use work.output_control_package.all;
 
 
entity register_instructiontb is
end register_instructiontb;
 
architecture behavior of register_instructionTB is 
 
    -- component declaration for the unit under test (uut)
 
    component register_instruction
    port(
         i_clk : in  STD_LOGIC;
         i_reset : in  STD_LOGIC;
         i_r_i_write : in  STD_LOGIC;
         i_instruction : in  STD_LOGIC_VECTOR(0 to 31);
         o_opcode : out  STD_LOGIC_VECTOR(0 to 5);
         o_jump_address : out  STD_LOGIC_VECTOR(0 to 25);
         o_rs : out  STD_LOGIC_VECTOR(0 to 4);
         o_rt : out  STD_LOGIC_VECTOR(0 to 4);
         o_rd : out  STD_LOGIC_VECTOR(0 to 4);
         o_shammt : out  STD_LOGIC_VECTOR(0 to 4);
         o_fcode : out  STD_LOGIC_VECTOR(0 to 5);
         o_immd : out  STD_LOGIC_VECTOR(0 to 15)
        );
    end component;
    

   --Inputs
   signal i_clk : STD_LOGIC := '0';
   signal i_reset : STD_LOGIC := '0';
   signal i_r_i_write : STD_LOGIC := '0';
   signal i_instruction : STD_LOGIC_VECTOR(0 to 31) := (others => '0');

 	--Outputs
   signal o_opcode : STD_LOGIC_VECTOR(0 to 5);
   signal o_jump_address : STD_LOGIC_VECTOR(0 to 25);
   signal o_rs : STD_LOGIC_VECTOR(0 to 4);
   signal o_rt : STD_LOGIC_VECTOR(0 to 4);
   signal o_rd : STD_LOGIC_VECTOR(0 to 4);
   signal o_shammt : STD_LOGIC_VECTOR(0 to 4);
   signal o_fcode : STD_LOGIC_VECTOR(0 to 5);
   signal o_immd : STD_LOGIC_VECTOR(0 to 15);

   -- Clock period definitions
   constant i_clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: register_instruction PORT MAP (
          i_clk => i_clk,
          i_reset => i_reset,
          i_r_i_write => i_r_i_write,
          i_instruction => i_instruction,
          o_opcode => o_opcode,
          o_jump_address => o_jump_address,
          o_rs => o_rs,
          o_rt => o_rt,
          o_rd => o_rd,
          o_shammt => o_shammt,
          o_fcode => o_fcode,
          o_immd => o_immd
        );

   -- Clock process definitions
   i_clk_process :process
   begin
		i_clk <= '0';
		wait for i_clk_period/2;
		i_clk <= '1';
		wait for i_clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin	
		wait for i_clk_period;
		
		i_instruction <= ADDI_OC & "10101" & "01010" & "0011001100110011";
		wait for i_clk_period;
		assert o_opcode = "000000" report "Geschrieben ohne i_r_write (vor reset)" severity error;
		
		i_r_i_write <= '1';
		wait for i_clk_period;
		assert o_opcode = ADDI_OC report "Opcode wurde nicht korrekt ausgegeben! (vor reset)" severity error;
		i_r_i_write <= '0';
		
      -- hold reset state for 100 ns.
		i_reset <= '1';
      wait for 100 ns;
		i_reset <= '0';
	
      wait for i_clk_period;
		
		i_instruction <= ADDI_OC & "10101" & "01010" & "0011001100110011";
		wait for i_clk_period;
		assert o_opcode = "000000" report "Geschrieben ohne i_r_write" severity error;
		
		i_r_i_write <= '1';
		wait for i_clk_period;
		assert o_opcode = ADDI_OC report "Opcode wurde nicht korrekt ausgegeben!" severity error;

    

      assert false report "Ende der Testbench!" severity failure;
   end process;

END;
