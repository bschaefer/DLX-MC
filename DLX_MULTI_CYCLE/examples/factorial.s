;------------------------------------------------------------------------------
;- Module: 						RAM initialisation factorial
;- Original author: 			bschaefer
;- Co-authors:
;- Last changed:				06.01.2016
;- Description:
;-
;------------------------------------------------------------------------------

main:
    addi r1,r0,5 ; <-- Parameter n for operation n!
    movi2fp r1,r1
    add r2,r0,r1

loop:
    subi r2,r2,1 ; next factor = n-i
    beqz r2, end
    movi2fp r2,r2
    mult r1,r1,r2 ; multiply previous result with next factor
    j loop
    
end:
    addi r3,r0,1 ; set finished flag
    j end