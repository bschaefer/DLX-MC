addi r1, r0, 0  ; Zahl 1
addi r2, r0, 1  ; Zahl 2
addi r3, r0, 20 ; Zähler
addi r4, r0, 400  ; Adresse
fib:
add r2, r1, r2
sw 0(r4), r1 ;
addi r4, r4, 4
subi r3, r3, 1    
beqz r3, end
   
add r1, r1, r2
sw 0(r4), r2
addi r4, r4, 4
subi r3, r3, 1
bnez r3, fib

end:
j end
