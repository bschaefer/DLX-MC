;###############################################################################
; Example Programm: Bubble-Sort

; An array of n numbers is stored in the ram, starting on the position r+1.
; n has to be stored on position r.
; e.g.
; pos | Value
; -----------
; r   | 11
; r+1 | -4
; r+2 | -1
; r+3 | 5
; r+4 | -5
; r+5 | -2
; r+6 | 3
; r+7 | 0
; r+8 | -3
; r+9 | 4
; r+10| 1
; r+11| 2
; The Example Programm sorts the values in ascending order.
; After that, mean and median are calculated and stored on position
; r+n+1 (mean)
; r+n+2 (median)
;###############################################################################

.text
j code_section ; jump to code-start

;###############################################################################
; Example Data
;###############################################################################
.data
data_section:
.word 12
.word -4
.word -1
.word 5
.word -5
.word -2
.word 3
.word 0
.word -3
.word 4
.word 1
.word 2
.word 4

;###############################################################################
; Main Program
;###############################################################################
.text
code_section:
addi r1, r0, data_section ; r1 = start address
lw r2, 0(r1) ; init counter r2 = number of values (n)

; r3: counter c1
; r4: counter c2
; r5: var1 value 1
; r6: var2 value 2
; r7: var3 tmp
; r8: var4 swapped

slli r4, r2, 2 ; c2 = n * 4
add r4, r4, r1 ; c2 = c2 + start address 


outer_loop: ; for(c2 = n; c2 > 0; --c2)
beqz r4, end_outer_loop

addi r3, r1, 4 ; c1 = start address + 4
add r8, r0, r0 ; swapped = 0 (false)

inner_loop: ; for(c1 = 1; c1 < c2; ++c1)
slt r7, r3, r4
beqz r7, end_inner_loop

; load values
lw r5, 0(r3); var1 = value[c1]
lw r6, 4(r3); var2 = value[c1 + 1 word]

; compare values
slt r7, r5, r6 ; r7 = (r5 < r6) ? 1 : 0

bnez r7, no_swap;
; swap and store values
sw 0(r3), r6
sw 4(r3), r5
addi r8, r0, 1 ; swapped = 1 (true)

no_swap:
; Increment c1 and loop
addi r3, r3, 4
j inner_loop

end_inner_loop:

; if !swapped => list is sorted
beqz r8, prog_end

; Decrement c2 and loop
subi r4, r4, 4
j outer_loop

end_outer_loop:

; endless loop
prog_end:
j prog_end


