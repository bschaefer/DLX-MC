;------------------------------------------------------------------------------
;- Module: 						RAM initialisation for system TB
;- Original author: 			bschaefer
;- Co-authors:
;- Last changed:				17.12.2015
;- Description:
;-	This file is used to initialize the RAM with (hopefully) all implemented
;-  instructions. It is further used by the "verifying_systemTB.vhd" testbench
;-  to assert the expected outputs.
;-
;------------------------------------------------------------------------------
.global main
main:
    j init
    addi r1,r0,-42
init:

;add
addi r0, r0, 666 ; r0 = 0
addi r1, r0, 42 ; r1 = 42
addi r2, r0, 12 ; r2 = 12
add r3, r2, r0 ; r3 = 12
add r3, r2, r1 ; r3 = 54

;subtract
subi r4, r1, 40 ; r4 = 2
subi r4, r2, 12 ; r4 = 0
sub r4, r3, r1 ; r4 = 12
sub r4, r4, r2 ; r4 = 0

;branch
beqz r4, ok_1
j error
ok_1:
bnez r3, ok_2
j error
ok_2:
beqz r1, error
bnez r8, error
j logical

error:
addi r1, r0, 666

logical:
and r4, r1, r2 ; r4 = 8
or r5, r1, r2 ; r5 = 46
xor r6, r1, r2 ; r6 = 38
andi r4, r4, 42 ; r4 = 8
ori r5, r5, 1 ; r5 = 47
xori r6, r6, 39 ; r6 = 1

;shift
sll r5, r5, r6 ; r5 = 94
srli r5, r5, 1 ; r5 = 47
srl r4, r4, r6 ; r4 = 4
slli r4, r4, 1 ; r4 = 8

;lw/sw
;breaks beginning code
sw 32(r0), r1 ; MEM(32) = 42
lw r7, 32(r0) ; r7 = 42

;multiplikation
movi2fp r1, r4 ; fp-r1 = 8
movi2fp r2, r7 ; fp-r2 = 42
mult r3, r1, r2 ; fp-r3 = 336
movfp2i r8, r3 ; int-r8 = 336

;comparisons
addi r8, r0, 7 ; r8 = 7
addi r9, r0, 103 ; r9 = 103
slt r10, r8, r9 ; r10 = 1
slt r10, r9, r8 ; r10 = 0
slt r10, r8, r9 ; r10 = 1
addi r8, r0, 103 ; r8 = 103
slt r10, r8, r9 ; r10 = 0
addi r8, r0, -1022; r8 = -1022
slt r10, r8, r9; r10 = 1
slt r10, r9, r8; r10 = 0



