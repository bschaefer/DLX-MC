-- TestBench Template 

  library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

  entity testbench is
  end testbench;

architecture behavior of testbench is 

  -- component declaration
component dlx_processor is
	generic(INITIALISATION: STRING);
	port( i_clk: 	in STD_LOGIC;
			i_reset: in STD_LOGIC );
end component;
          
signal clk : STD_LOGIC := '0'; 
signal reset : STD_LOGIC := '0';

constant CLK_PERIOD : TIME := 20 ns;
begin

  -- component instantiation
          uut: dlx_processor 
			 generic map(
				INITIALISATION => "examples/bubblesort.ram"
			 )
			 port map(
                  i_clk => clk,
                  i_reset => reset
          );

clock : process 
begin
	clk <= '0';
	wait for CLK_PERIOD/2;
	clk <= '1';
	wait for CLK_PERIOD/2;
end process;

end;
