-------------------------------------------------------------------------------
-- DLX_MULTI_CYCLE (DLX-MC)
-- Copyright 2016
-- https://gitlab.com/bschaefer/DLX-MC
--
-- This file is part of DLX-MC.
-- 
-- DLX-MC is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- DLX-MC is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with DLX-MC.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------
-- Module: 						dlx_processor_testing
-- Original author: 			ptschubij (dlx_processor)
-- Co-authors:					bschaefer
-- Last changed:				17.12.2015
-- Description:
--	The Top-Module of the project. It describes the whole processor with its 
-- cabling and multiplexors.
-- extended entity for testing
--
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.dlx_package.all;


entity dlx_processor_testing is
	generic(INITIALISATION: STRING);
	port( i_clk: 			in STD_LOGIC;
			i_reset: 		in STD_LOGIC;
			o_ram: 			out T_RAM;
			o_registers:	out T_GP_REGISTERS;
			o_fp_registers:out T_GP_REGISTERS;
			o_pc:				out STD_LOGIC_VECTOR(0 to 31);
			o_state_nr:		out INTEGER
			);
end dlx_processor_testing;


architecture Behavioral of dlx_processor_testing is

component alu is
	port(
		i_op_a: 		 				in  STD_LOGIC_VECTOR(0 to 31);
		i_op_b:  	 				in  STD_LOGIC_VECTOR(0 to 31);
		i_operation: 				in  T_ALU_OPERATION;
		o_flags: 	 				out T_ALU_FLAGS;
		o_result: 	 				out STD_LOGIC_VECTOR(0 to 31)
	);
end component;

component alu_control is
	port(
		i_instruction: 			in STD_LOGIC_VECTOR(0 to 5);
		i_alu_op: 					in T_ALU_OP;
		o_operation: 				out T_ALU_OPERATION;
		o_register_source: 		out T_REGISTER_SELECTION;
		o_register_destination:	out T_REGISTER_SELECTION
	);
end component;

component memory_testing is
	generic(INITIALISATION: STRING);
	port(
		i_reset:						in  STD_LOGIC;
		i_write_enable:			in  STD_LOGIC;
		i_read_data:	 			in  STD_LOGIC;
		i_write_data:  			in  STD_LOGIC_VECTOR(0 to 31);
		i_address: 					in  STD_LOGIC_VECTOR(0 to 31);
		o_mem_data: 			out STD_LOGIC_VECTOR(0 to 31);
		o_ram:					out T_RAM
	);
end component;

component output_control_testing is
	port(
		i_clk:						in  STD_LOGIC;
		i_opcode: 					in  STD_LOGIC_VECTOR(0 to 5);
		o_pc_write_cond:  		out STD_LOGIC;
		o_inv_zero_flag:			out STD_LOGIC;
		o_pc_write: 				out STD_LOGIC;
		o_i_or_d: 					out T_I_OR_D; -- instruction or data
		o_mem_read: 				out STD_LOGIC;
		o_mem_write: 				out STD_LOGIC;
		o_mem_to_reg: 				out STD_LOGIC;
		o_r_i_write:				out STD_LOGIC; -- IRWrite
		o_pc_source: 				out T_PC_SOURCE;
		o_alu_op: 					out T_ALU_OP;
		o_alu_src_b: 				out T_ALU_SRC_B;
		o_alu_src_a: 				out T_ALU_SRC_A;
		o_reg_write: 				out STD_LOGIC;
		o_reg_dst: 				out T_REG_DST;
		o_state_nr:				out INTEGER
	);
end component;

component pc is
	port(
		i_clk: 	 					in  STD_LOGIC;
		i_reset:						in  STD_LOGIC;
		i_address: 					in  STD_LOGIC_VECTOR(0 to 31);
		i_enable: 					in  STD_LOGIC;
		o_address: 					out STD_LOGIC_VECTOR(0 to 31)
	);
end component;

component register_array_testing is
	port(
		i_clk: 						in  STD_LOGIC;
		i_reset:						in  STD_LOGIC;
		i_read_register1: 		in  STD_LOGIC_VECTOR(0 to 4);
		i_read_register2: 		in  STD_LOGIC_VECTOR(0 to 4);
		i_write_register: 		in  STD_LOGIC_VECTOR(0 to 4);
		i_write_data: 				in  STD_LOGIC_VECTOR(0 to 31);
		i_reg_write: 				in  STD_LOGIC;
		o_read_data1: 				out STD_LOGIC_VECTOR(0 to 31);
		o_read_data2: 			out STD_LOGIC_VECTOR(0 to 31);
		o_registers:			out T_GP_REGISTERS
	);	
end component;

component register_general is
	port(
		i_clk: 						in  STD_LOGIC;
		i_reset:						in  STD_LOGIC;
		i_en: 						in  STD_LOGIC;
		i_data: 						in  STD_LOGIC_VECTOR(0 to 31);
		o_data: 						out STD_LOGIC_VECTOR(0 to 31)
	);
end component;

component register_instruction is
	port(
		i_clk: 						in  STD_LOGIC;
		i_reset:						in  STD_LOGIC;
		i_r_i_write: 				in  STD_LOGIC;
		i_instruction: 			in  STD_LOGIC_VECTOR(0 to 31);
		o_opcode:					out STD_LOGIC_VECTOR(0 to 5);
		o_jump_address: 			out STD_LOGIC_VECTOR(0 to 25);
		o_rs:							out STD_LOGIC_VECTOR(0 to 4);
		o_rt: 						out STD_LOGIC_VECTOR(0 to 4);
		o_rd: 						out STD_LOGIC_VECTOR(0 to 4);
		o_shammt: 					out STD_LOGIC_VECTOR(0 to 4);
		o_fcode: 					out STD_LOGIC_VECTOR(0 to 5);
		o_immd: 						out STD_LOGIC_VECTOR(0 to 15)
	);
end component;

component register_md is
	port(
		i_clk:						in  STD_LOGIC;
		i_data: 						in  STD_LOGIC_VECTOR(0 to 31);
		o_data: 						out STD_LOGIC_VECTOR(0 to 31)
	);	
end component;	
	
component shift_left_2 is	
	port(	
		i_data: 						in  STD_LOGIC_VECTOR(0 to 31);
		i_instruction: 			in  STD_LOGIC_VECTOR(0 to 25);
		o_data: 						out STD_LOGIC_VECTOR(0 to 31);
		o_instruction: 			out STD_LOGIC_VECTOR(0 to 27)
	);
end component;

component sign_extend is
	port( 
		i_instruction_16: 		in  STD_LOGIC_VECTOR(0 to 15);
		o_instruction_32:			out STD_LOGIC_VECTOR(0 to 31)
	);
end component;

-- signals:

-- ALU Control
signal w_operation: 				T_ALU_OPERATION;
signal w_register_source:		T_REGISTER_SELECTION;
signal w_register_destination:T_REGISTER_SELECTION;
-- ALU	
signal w_flags: 					T_ALU_FLAGS;
signal w_result: 					STD_LOGIC_VECTOR(0 to 31);
-- Memory	
signal w_mem_data: 				STD_LOGIC_VECTOR(0 to 31);
-- Control Unit	
signal w_pc_write_cond: 		STD_LOGIC;
signal w_inv_zero_flag:			STD_LOGIC;
signal w_pc_write: 				STD_LOGIC;
signal w_i_or_d: 					T_I_OR_D;
signal w_mem_read: 				STD_LOGIC;
signal w_mem_write: 				STD_LOGIC;
signal w_mem_to_reg: 			STD_LOGIC;
signal w_r_i_write: 				STD_LOGIC;
signal w_pc_source: 				T_PC_SOURCE;
signal w_alu_op: 					T_ALU_OP;
signal w_alu_src_b: 				T_ALU_SRC_B;
signal w_alu_src_a: 				T_ALU_SRC_A;
signal w_reg_write: 				STD_LOGIC;
signal w_i_reg_write:			STD_LOGIC;
signal w_fp_reg_write:			STD_LOGIC;
signal w_reg_dst: 				T_REG_DST;
-- PC	
signal w_address: 				STD_LOGIC_VECTOR(0 to 31);
-- Main Register
signal w_read_i_data1:			STD_LOGIC_VECTOR(0 to 31);
signal w_read_i_data2:			STD_LOGIC_VECTOR(0 to 31);
-- Floating point register	
signal w_read_fp_data1:			STD_LOGIC_VECTOR(0 to 31);
signal w_read_fp_data2:			STD_LOGIC_VECTOR(0 to 31);
-- Register (A, B, ALUOut)	
signal w_reg_a_data: 			STD_LOGIC_VECTOR(0 to 31);
signal w_reg_b_data:				STD_LOGIC_VECTOR(0 to 31);
signal w_fp_reg_a_data:			STD_LOGIC_VECTOR(0 to 31);
signal w_fp_reg_b_data:			STD_LOGIC_VECTOR(0 to 31);
signal w_reg_alu_out_data:		STD_LOGIC_VECTOR(0 to 31);
-- Instruction Register	
signal w_opcode: 					STD_LOGIC_VECTOR(0 to 5); -- [0..5]
signal w_jump_address: 			STD_LOGIC_VECTOR(0 to 25); -- [6..31]
signal w_rs: 						STD_LOGIC_VECTOR(0 to 4); -- [6..10]
signal w_rt: 						STD_LOGIC_VECTOR(0 to 4); -- [11..15]
signal w_rd: 						STD_LOGIC_VECTOR(0 to 4); -- [16..20]
signal w_shammt: 					STD_LOGIC_VECTOR(0 to 4); -- [21..25]
signal w_fcode: 					STD_LOGIC_VECTOR(0 to 5); -- [26...31]
signal w_immd: 					STD_LOGIC_VECTOR(0 to 15); -- [16..31]
-- Memory Data Register (not for Instruction Register which is directly connected to memory)
signal w_reg_md_data: 			STD_LOGIC_VECTOR(0 to 31);
-- Shift Left 2	
signal w_shifted_immd: 			STD_LOGIC_VECTOR(0 to 31);
signal w_shifted_j_a:			STD_LOGIC_VECTOR(0 to 27);
-- Sign Extend	
signal w_immd_32: 				STD_LOGIC_VECTOR(0 to 31);
-- Multiplexor	
signal mux_memory_address:		STD_LOGIC_VECTOR(0 to 31);
signal mux_write_register:		STD_LOGIC_VECTOR(0 to 4);
signal mux_write_data:			STD_LOGIC_VECTOR(0 to 31);
signal mux_op_a:					STD_LOGIC_VECTOR(0 to 31);
signal mux_op_b:					STD_LOGIC_VECTOR(0 to 31);
signal mux_pc_address:			STD_LOGIC_VECTOR(0 to 31);
-- Full Jump Address	
signal w_full_jump_address:	STD_LOGIC_VECTOR(0 to 31);
-- PC Enable	
signal w_pc_enable:				STD_LOGIC;


begin

-- instantiation
I_ALU_CONTROL: alu_control
	port map( 
		i_instruction => w_fcode,
		i_alu_op => w_alu_op,
		o_operation => w_operation,			
		o_register_source => w_register_source, 		
		o_register_destination => w_register_destination		
	);
				 
I_ALU: alu
	port map( 
		i_op_a => mux_op_a,
		i_op_b => mux_op_b,
		i_operation => w_operation,
		o_flags => w_flags,
		o_result => w_result
	);
	
I_FP_REGISTERS: register_array_testing
	port map(
		i_clk => i_clk,
		i_reset => i_reset,
		i_read_register1 => w_rs,
		i_read_register2 => w_rt,
		i_write_register => mux_write_register,
		i_write_data => mux_write_data,
		i_reg_write => w_fp_reg_write,
		o_read_data1 => w_read_fp_data1,
		o_read_data2 => w_read_fp_data2,
		o_registers => o_fp_registers
	);
	
I_FP_REGISTER_A: register_general
	port map(
		i_clk => i_clk,
		i_reset => i_reset,
		i_en => '1',
		i_data => w_read_fp_data1,
		o_data => w_fp_reg_a_data
	);

I_FP_REGISTER_B: register_general
	port map(
		i_clk => i_clk,
		i_reset => i_reset,
		i_en => '1',
		i_data => w_read_fp_data2,
		o_data => w_fp_reg_b_data
	);
				 
I_MEMORY: memory_testing
	generic map(
		INITIALISATION => INITIALISATION
	)
	port map(
		i_reset => i_reset,
		i_write_enable => w_mem_write,
		i_read_data => w_mem_read,
		i_write_data => w_reg_b_data,
		i_address => mux_memory_address,
		o_mem_data => w_mem_data,
		o_ram => o_ram
	);
				 
I_OCU: output_control_testing
	port map(
		i_clk => i_clk,
		i_opcode => w_opcode,
		o_pc_write_cond => w_pc_write_cond,
		o_inv_zero_flag => w_inv_zero_flag,
		o_pc_write => w_pc_write,
		o_i_or_d => w_i_or_d,
		o_mem_read => w_mem_read,
		o_mem_write => w_mem_write,
		o_mem_to_reg => w_mem_to_reg,
		o_r_i_write => w_r_i_write,
		o_pc_source => w_pc_source,
		o_alu_op => w_alu_op,
		o_alu_src_b => w_alu_src_b,
		o_alu_src_a => w_alu_src_a,
		o_reg_write => w_reg_write,
		o_reg_dst => w_reg_dst,
		o_state_nr => o_state_nr
	);
	
I_PC: pc
	port map(
		i_clk => i_clk,
		i_reset => i_reset,
		i_address => mux_pc_address,
		i_enable => w_pc_enable,
		o_address => w_address
	);
	
I_MAIN_REGISTER: register_array_testing
	port map(
		i_clk => i_clk,
		i_reset => i_reset,
		i_read_register1 => w_rs,
		i_read_register2 => w_rt,
		i_write_register => mux_write_register,
		i_write_data => mux_write_data,
		i_reg_write => w_i_reg_write,
		o_read_data1 => w_read_i_data1,
		o_read_data2 => w_read_i_data2,
		o_registers => o_registers
	);
	
I_REGISTER_A: register_general
	port map(
		i_clk => i_clk,
		i_reset => i_reset,
		i_en => '1',
		i_data => w_read_i_data1,
		o_data => w_reg_a_data
	);

I_REGISTER_B: register_general
	port map(
		i_clk => i_clk,
		i_reset => i_reset,
		i_en => '1',
		i_data => w_read_i_data2,
		o_data => w_reg_b_data
	);
	
I_REGISTER_ALU_OUT: register_general
	port map(
		i_clk => i_clk,
		i_reset => i_reset,
		i_en => '1',
		i_data => w_result,
		o_data => w_reg_alu_out_data
	);

I_INSTRUCTION_REGISTER: register_instruction
	port map(
		i_clk => i_clk,
		i_reset => i_reset,
		i_r_i_write => w_r_i_write,
		i_instruction => w_mem_data,
		o_opcode => w_opcode,
		o_jump_address => w_jump_address,
		o_rs => w_rs,
		o_rt => w_rt,
		o_rd => w_rd,
		o_shammt => w_shammt,
		o_fcode => w_fcode,
		o_immd => w_immd
	);
	
I_MEMORY_DATA_REGISTER: register_md
	port map(
		i_clk => i_clk,
		i_data => w_mem_data,
		o_data => w_reg_md_data
	);
	
I_SHIFT_LEFT_2: shift_left_2
	port map(
		i_data => w_immd_32,
		i_instruction => w_jump_address,
		o_data => w_shifted_immd,
		o_instruction => w_shifted_j_a
	);
	
I_SIGN_EXTEND: sign_extend
	port map(
		i_instruction_16 => w_immd,
		o_instruction_32 => w_immd_32
	);

-- Multiplexor
memory_adress_multiplexor:
mux_memory_address <= 
    w_address 				when w_i_or_d = instruction else
    w_reg_alu_out_data 	when w_i_or_d = data;
    
write_register_multiplexor:
mux_write_register <=
	w_rt 						when w_reg_dst = t else
	w_rd 						when w_reg_dst = d;

write_data_multiplexor:
mux_write_data <=
	w_reg_alu_out_data 	when w_mem_to_reg = '0' else
	w_reg_md_data 		 	when w_mem_to_reg = '1';
    
op_a_multiplexor:
mux_op_a <=
	w_address 				when w_alu_src_a = T_ALU_SRC_A'VALUE("pc") else
	w_reg_a_data 			when w_alu_src_a = reg_a AND w_register_source = integer_register else
	w_fp_reg_a_data		when w_alu_src_a = reg_a AND w_register_source = floating_register;
    
op_b_multiplexor:
mux_op_b <=
	w_reg_b_data 			when w_alu_src_b = reg_b AND w_register_source = integer_register else
	w_fp_reg_b_data		when w_alu_src_b = reg_b AND w_register_source = floating_register else
	VECTOR_FOUR				when w_alu_src_b = T_ALU_SRC_B'VALUE("four") else
	w_immd_32 				when w_alu_src_b = immediate else
	w_shifted_immd 		when w_alu_src_b = i_address;
    
pc_address_multiplexor:
mux_pc_address <=
	w_result 				when w_pc_source = alu_result else
	w_reg_alu_out_data	when w_pc_source = alu_out else
	w_full_jump_address 	when w_pc_source = jmp_adr else
	VECTOR_ZERO 			when w_pc_source = const;
	

-- Multiple register destination handling (gp/int and fp)
reg_write_gate:
process (w_register_destination, w_reg_write) is
begin
	if w_register_destination = integer_register then
		w_i_reg_write 	<= w_reg_write;
		w_fp_reg_write <= '0';
	elsif w_register_destination = floating_register then
		w_i_reg_write 	<= '0';
		w_fp_reg_write <= w_reg_write;
	end if;
end process;
	
	
-- Full Jump Address
w_full_jump_address <=
	w_address(0 to 3) & w_shifted_j_a;

-- PC Enable
w_pc_enable <= 
	((w_flags.zero XOR w_inv_zero_flag) AND w_pc_write_cond)
	OR
	w_pc_write;

testing_output:
	o_pc <= w_address;
	

end Behavioral;