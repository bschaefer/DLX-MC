-------------------------------------------------------------------------------
-- DLX_MULTI_CYCLE (DLX-MC)
-- Copyright 2016
-- https://gitlab.com/bschaefer/DLX-MC
--
-- This file is part of DLX-MC.
-- 
-- DLX-MC is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- DLX-MC is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with DLX-MC.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------
-- Module: 						sign_extend
-- Original author: 			ptschubij
-- Co-authors:					
-- Last changed:				12.05.2015
-- Description:
--	Extends an input STD_LOGIC_VECTOR to an output STD_LOGIC_VECTOR of
-- different length. In this case it's used for the immediate operand.
--
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity sign_extend is
	port( 
		i_instruction_16: in 	STD_LOGIC_VECTOR(0 to 15);
		o_instruction_32:	out 	STD_LOGIC_VECTOR(0 to 31)
	);
end sign_extend;


architecture Behavioral of sign_extend is
begin
	o_instruction_32 <= STD_LOGIC_VECTOR(resize(signed(i_instruction_16),--
	o_instruction_32'length));

end Behavioral;